#-------------------------------------------------
#
# Project created by QtCreator 2017-12-05T15:30:20
#
#-------------------------------------------------

QT       -= gui
QT      += quick serialport bluetooth

TARGET = lib
TEMPLATE = lib

DEFINES += LIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        lib.cpp \
        serial/serialcomm.cpp \
        serial/serialport.cpp \
        serial/bluetoothport.cpp \
        platform.cpp


HEADERS += \
        lib.h \
        lib_global.h \
        serial/serialcomm.h \
        serial/serialport.h \
        serial/bluetoothport.h \
        platform.h

DISTFILES += \
    qml/component/BackgroundPanel.qml \
    qml/component/RButton.qml \
    qml/component/RCloseButton.qml \
    qml/component/RDelayButton.qml \
    qml/component/RDoubleSpinBox.qml \
    qml/component/RDoubleSpinText.qml \
    qml/component/RIconButton.qml \
    qml/component/RMessageBox.qml \
    qml/component/RRadioButton.qml \
    qml/component/RSpinBox.qml \
    qml/component/RSpinner.qml \
    qml/component/RStageVisualizer.qml \
    qml/component/RToastMessage.qml \
    qml/qmldir \
    qml/component/RToolTip.qml \
    qml/Connection.qml \
    qml/Responsive.qml \
    qml/qmldir \
    qml/component/qmldir \
    qml/PageTemplate.qml \
    qml/component/RDialogTemplate.qml \
    qml/component/RTextArea.qml \
    qml/component/RTextField.qml \
    qml/component/RSwitch.qml

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    shared-qml.qrc
