#ifndef ISERIALINTERFACE_H
#define ISERIALINTERFACE_H

#include <QObject>
#include <QIODevice>
#include <QTimer>
#include <QQueue>
#include <QTime>
#include <QQuickItem>

class SerialComm : public QQuickItem    {

    Q_OBJECT
    Q_ENUMS(ConnectionState)
    Q_ENUMS(BaudRate)
    Q_PROPERTY(ConnectionState state READ getState NOTIFY stateChanged)
    Q_PROPERTY(int bufferedCommandsLength READ getBufferedCommandsLength)
    Q_PROPERTY(QVariantMap connectingDevice READ connectingDevice NOTIFY connectingDeviceChanged)
    Q_PROPERTY(bool debugEnabled READ debugEnabled WRITE setDebugEnabled NOTIFY debugEnabledChanged)
    Q_PROPERTY(BaudRate baud READ baudRate WRITE setBaudRate NOTIFY baudRateChanged)

public:

    enum ConnectionState{ Disconnected, Connecting , Connected , Discovering , Error , None };
    enum BaudRate { B115200 , B9600 };

    static void         registerQmlType();
    SerialComm(QQuickItem* parent = nullptr);
    int                 getBufferedCommandsLength();
    ConnectionState     getState();
    BaudRate            baudRate();
    QVariantMap         connectingDevice();
    bool                debugEnabled();
    void                setDebugEnabled(bool val);
    void                setBaudRate(BaudRate val);

protected:

    void    sendDeviceDiscoveredSignal(QString &name , QString &address);
    void    sendConnectionStatusChangedSignal();
    void    sendConnectingStatusChangedSignal();
    void    sendScanStatusChangedSignal();
    void    sendDataIncomeSignal(QString &data);
    void    sendStateChangedSignal();
    void    setSocket(QIODevice *dev);
    void    setState(ConnectionState state);
    void    setConnectingDevice(QVariantMap name);

signals:

    //Property signals
    void    debugEnabledChanged();
    void    baudRateChanged();

    void    deviceDiscovered(QString name , QString address);
    void    connectionStatusChanged();
    void    connectingStatusChanged();
    void    scanStatusChanged();
    void    dataIncome(QString data);
    void    stateChanged();
    void    connectingDeviceChanged();
    void    scanStarted();
    void    scanFinished();

public slots:

    virtual void    connectTo(QString address);
    virtual void    closeSocket();
    virtual void    startScan();
    virtual void    send(QString text);
    virtual void    write(QByteArray array);
    QIODevice*      device();
    void            clearBuffer();

private slots:

    void sendTimerTick();

protected:

    QIODevice*      m_socket;

private:

    QQueue<QString>     m_buffer;
    QTimer              m_senderTimer;
    QTime               m_lastTick;
    ConnectionState     m_state;
    BaudRate            m_baudRate;
    QVariantMap         m_connectingDevice;
    bool                m_debugEnabled;

};

#endif // ISERIALINTERFACE_H
