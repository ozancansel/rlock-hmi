#ifndef BLUETOOTHUTIL_H
#define BLUETOOTHUTIL_H

#include <QObject>
#include <QBluetoothSocket>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothLocalDevice>
#include <QList>
#include <QTimer>
#include <QTime>
#include <QQueue>
#include <QQuickItem>
#include "serialcomm.h"

class BluetoothPort : public  SerialComm
{

    Q_OBJECT

public:

    static void registerQmlType();
    BluetoothPort(QQuickItem* parent = nullptr);

public slots:

    void            connectTo(QString address) override;
    void            startScan() override;

private slots:

    void            btConnected();
    void            btDisconnected();
    void            btDeviceDiscovered(const QBluetoothDeviceInfo&);
    void            btScanFinished();
    void            btStateChanged(QBluetoothSocket::SocketState state);

private:

    QList<QBluetoothDeviceInfo>         m_discoveredDevices;
    QMap<QString , QBluetoothDeviceInfo*>       _discoveredDevices;
    QBluetoothDeviceDiscoveryAgent      *m_agent;
    QBluetoothLocalDevice               *m_localDevice;
    QBluetoothSocket                    *_socket;

    void                    appendIfNotExists(QBluetoothDeviceInfo info);
    QBluetoothDeviceInfo    byAddress(QString addr);

};

#endif // BLUETOOTHUTIL_H
