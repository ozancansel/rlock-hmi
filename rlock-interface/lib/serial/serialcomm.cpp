#include "serialcomm.h"
#include <QDebug>
#include <QQuickItem>

void SerialComm::registerQmlType(){
    qmlRegisterType<SerialComm>("RLib" , 1 , 0 , "SerialComm");
}

SerialComm::SerialComm(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_state(None) ,
      m_debugEnabled(false) ,
      m_baudRate(B115200)
{
    connect(&m_senderTimer , SIGNAL(timeout()) , this , SLOT(sendTimerTick()));
}
void    SerialComm::sendDeviceDiscoveredSignal(QString &name , QString &address){
    emit deviceDiscovered(name , address);
}
void    SerialComm::sendConnectionStatusChangedSignal(){
    emit connectionStatusChanged();
}
void    SerialComm::sendConnectingStatusChangedSignal(){
    emit connectingStatusChanged();
}
void    SerialComm::sendScanStatusChangedSignal(){
    emit scanStatusChanged();
}
void    SerialComm::sendDataIncomeSignal(QString &data){
    emit dataIncome(data);
}
void    SerialComm::sendStateChangedSignal(){
    emit stateChanged();
}
void SerialComm::setSocket(QIODevice *dev){
    m_socket = dev;
}
void SerialComm::setState(ConnectionState state){
    m_state  = state;

    if(m_state == Discovering)
        emit scanStatusChanged();

    emit stateChanged();
}
void SerialComm::send(QString text){
    if(debugEnabled())
        qDebug() << "SerialComm::send(QString text) => " << text;
    if(m_buffer.empty() && QTime::currentTime().msec() - m_lastTick.msec() > m_senderTimer.interval()){
        m_socket->write(text.toLatin1());
        return;
    }   else    {
        if(!m_senderTimer.isActive())
            m_senderTimer.start();

        m_buffer.enqueue(text);
    }
}

void SerialComm::write(QByteArray array){

    //Eğer ki bağlantı yoksa, buffer temizleniyor
    if(!m_socket->isOpen())
        return;

    m_socket->write(array);
    m_lastTick = QTime::currentTime();
}

void SerialComm::closeSocket(){
    m_socket->close();
    setState(None);
}

void SerialComm::sendTimerTick(){
    //Eğer ki buffer boşsa timer durduruluyor
    if(m_buffer.isEmpty()){
        m_senderTimer.stop();
        return;
    }

    //Eğer ki bağlantı yoksa, buffer temizleniyor
    if(!m_socket->isOpen()){

        m_senderTimer.stop();
        m_buffer.clear();

        return;
    }

    if(!m_socket->isOpen())
        return;

    QString command = m_buffer.dequeue();
    m_socket->write(command.toLatin1());
    m_lastTick = QTime::currentTime();
}

int SerialComm::getBufferedCommandsLength(){
    return m_buffer.length();
}

SerialComm::ConnectionState SerialComm::getState(){
    return m_state;
}

QIODevice* SerialComm::device(){
    return m_socket;
}

SerialComm::BaudRate SerialComm::baudRate(){
    return m_baudRate;
}

bool SerialComm::debugEnabled(){
    return m_debugEnabled;
}

void SerialComm::setDebugEnabled(bool val){
    m_debugEnabled = val;
    emit debugEnabledChanged();
}

void SerialComm::setBaudRate(BaudRate val){
    m_baudRate = val;
    emit baudRateChanged();
}

void SerialComm::connectTo(QString address){
    Q_UNUSED(address)
}

void SerialComm::startScan(){

}

QVariantMap SerialComm::connectingDevice(){
    return m_connectingDevice;
}

void SerialComm::setConnectingDevice(QVariantMap name){
    m_connectingDevice = name;
    emit connectingDeviceChanged();
}

void SerialComm::clearBuffer(){
    device()->readAll();
}
