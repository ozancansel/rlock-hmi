#include "serialport.h"
#include <QDebug>
#include <QSerialPortInfo>

void SerialPort::registerQmlType()  {
    qmlRegisterType<SerialPort>("RLib" , 1 , 0 , "SerialPort");
}

SerialPort::SerialPort(QQuickItem* parent) : SerialComm(parent)
{
    setSocket(&m_port);
    connect(&m_port , SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this , SLOT(errorOccurred(QSerialPort::SerialPortError)));

    PortInfo    uno;
    PortInfo    mega;

    uno.board = "Uno";
    uno.pairs << IdPair(0x2341 , 0x0043) << IdPair(0x2341 , 0x0001)
              << IdPair(0x2A03 , 0x0043) << IdPair(0x2341 , 0x0243)
              << IdPair(0x1A86 , 0x7523);

    mega.board = "Mega";
    mega.pairs << IdPair(9025 , 66);

    m_definedPorts.append(uno);
    m_definedPorts.append(mega);

    m_baudRateMap.insert(SerialComm::B9600 , QSerialPort::Baud9600);
    m_baudRateMap.insert(SerialComm::B115200 , QSerialPort::Baud115200);
}

void SerialPort::connectTo(QString address){

    QVariantMap map;
    map["name"] = address;
    map["address"] = address;
    setConnectingDevice(map);
    m_port.setPortName(address);

    setState(Connecting);
    if(!m_port.open(QSerialPort::ReadWrite)){
        qDebug() << "Seri Port acilamadi";
        setState(None);
        return;
    } else {
        qDebug() << "Porta baglandi.";
        setState(Connected);
    }

    m_port.setBaudRate(m_baudRateMap.value(baudRate()));
    m_port.setDataBits(QSerialPort::Data8);
    m_port.setParity(QSerialPort::NoParity);
    m_port.setStopBits(QSerialPort::OneStop);
    m_port.setFlowControl(QSerialPort::NoFlowControl);
}

void SerialPort::startScan(){

    emit scanStarted();

    QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();

    foreach (QSerialPortInfo info, infos) {
        int vid = info.vendorIdentifier();
        int pid = info.productIdentifier();
        QString boardName = retrieveBoardName(info.vendorIdentifier() , info.productIdentifier());
        QString pName = info.portName();

        //Eger board name geri donmediyse
        if(boardName.isEmpty())
            continue;

        sendDeviceDiscoveredSignal(boardName, pName);
    }

    emit scanFinished();
}

void SerialPort::errorOccurred(QSerialPort::SerialPortError err){
    if(err != QSerialPort::NoError && err != QSerialPort::TimeoutError)
        setState(Disconnected);
}

QString SerialPort::retrieveBoardName(int vid, int pid){

    foreach (PortInfo info, m_definedPorts) {
        foreach (IdPair pair, info.pairs) {
            if(pair.pid == pid && pair.vid == vid)
                return info.board;
        }
    }

    return QString();
}
