import QtQuick 2.0
import QtQuick.Controls 2.1
import "../"

SpinBox{
    property real factor        :   Math.pow(10, decimals)
    property int  decimals      :   1
    property real realValue     :   0.0
    property real realFrom      :   0.0
    property real realTo        :   100.0
    property real realStepSize  :   1.0
    readonly property color     buttonColor :   "#191919"
    readonly property color     pressedColor:   "#A8A8A8"
    id          :   spinbox
    stepSize    :   realStepSize*factor
    value       :   realValue*factor
    to          :   realTo*factor
    from        :   realFrom*factor
    editable    :   true

    function    getVal(){
        return value / factor
    }

    up.indicator  :   Item{
        property real borderRadius  :   Math.min(height , width) * 0.2
        width               :   spinbox.width * 0.2
        height              :   spinbox.height
        anchors.right       :   spinbox.right

        Rectangle{
            id              :   upBack
            anchors.fill    :   parent
            color           :   up.pressed ? pressedColor : buttonColor
        }

        Rectangle{
            anchors.left    :   parent.left
            width           :   upBack.radius
            height          :   parent.height
            color           :   upBack.color
        }

        Text {
            id              :   upText
            text            :   "+"
            anchors.centerIn:   parent
            color           :   "white"
            font.pixelSize  :   Math.min(parent.width , parent.height) * 0.45
        }
    }

    down.indicator  :    Item{
        property real borderRadius  :   Math.min(height , width) * 0.2
        width               :   spinbox.width * 0.2
        height              :   spinbox.height
        anchors.left        :   spinbox.left

        Rectangle{
            id              :   downBack
            anchors.fill    :   parent
            color           :   down.pressed ? pressedColor : buttonColor
        }

        Rectangle{
            anchors.left    :   parent.left
            width           :   downBack.radius
            height          :   parent.height
            color           :   upBack.color
        }

        Text {
            id              :   downText
            text            :   "-"
            anchors.centerIn:   parent
            color           :   "white"
            font.pixelSize  :   Math.min(parent.width , parent.height) * 0.45
        }
    }

    background  :   Rectangle {
        height  :   parent.height
        anchors.left    :   down.indicator.right
        anchors.right   :   up.indicator.left
    }

    validator   :   DoubleValidator {
        bottom  :   Math.min(spinbox.from, spinbox.to)  *   spinbox.factor
        top     :   Math.max(spinbox.from, spinbox.to)    *   spinbox.factor
    }

    textFromValue: function(value, locale) {
        var val = parseFloat(value * 1.0/factor).toFixed(decimals);
        return val;
    }

    valueFromText:  function(value , local){
        var val = parseFloat(value) * factor
        return val
    }
}
