import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import RLib 1.0
import RLock 1.0
import "shared_qml"
import "qml"

ApplicationWindow {
    readonly property SerialComm    port    :   Platform.isMobile ? bt : serial

    id          :   mainWindow
    visible     :   true
    visibility  :   Window.FullScreen
//    width       :   540
//    height      :   960
    title       :   qsTr("R&G Steel Doors")

    onClosing           :   appSettings.save()

    BluetoothPort   {
        id              :   bt
        debugEnabled    :   false
    }

    SerialPort  {
        id              :   serial
        debugEnabled    :   false
        baud            :   SerialComm.B9600
    }

    background  :   Background{
        width   :   mainWindow.width
        height  :   mainWindow.height
    }

    RLockProxy{
        id      :   lockProxy
        comm    :   port
    }

    AppSettings{
        id      :   appSettings
    }

//    BluetoothListener   {
//        id      :   listener
//        comm    :   port
//    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex    :   0

        UnlockPage {
            proxy           :   lockProxy
        }

        PortraitConnection {
            comm            :   port
            isCurrentPage   :   swipeView.currentIndex == 1
            settings        :   appSettings
        }

        Settings    {
            settings        :   appSettings
        }

        Component.onCompleted: {
            //Swipe to bluetooth page
            swipeView.setCurrentIndex(1)
        }
    }


    Connections{
        target  :   port
        onStateChanged  :   {
            if(port.state == SerialComm.Connected){
                swipeView.setCurrentIndex(0)
            }
        }
    }

    footer  :   Item    {
        height      :   40
        width       :   mainWindow.width
        PageIndicator{
            id              :   pageIndicator
            count           :   swipeView.count
            currentIndex    :   swipeView.currentIndex
            anchors.centerIn:   parent
            delegate        :   Rectangle{
                implicitWidth   :   20
                implicitHeight  :   20
                radius          :   width / 2
                color           :   "white"
                opacity         :   index == pageIndicator.currentIndex ? 0.95 : pressed ? 0.7 : 0.45
            }
        }
    }
}
