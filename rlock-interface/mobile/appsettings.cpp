#include "appsettings.h"
#include <QQuickItem>

void AppSettings::registerQmlType(){
    qmlRegisterType<AppSettings>("RLock" , 1 , 0 , "AppSettings");
}

AppSettings::AppSettings(QObject* parent)
    :
      QSettings(parent)
{
    setDefaultFormat(QSettings::IniFormat);
}

//Getter
bool AppSettings::autoConnect(){
    return value(LABEL_AUTO_CONNECT).toBool();
}

QString AppSettings::autoConnectAddress(){
    return value(LABEL_AUTO_CON_ADDR).toString();
}

QString AppSettings::lastConnectedDevice(){
    return value(LABEL_LAST_CONNECTED_DEV).toString();
}

//Setter
void AppSettings::setAutoConnect(bool enabled){
    setValue(LABEL_AUTO_CONNECT , QVariant(enabled));
    emit autoConnectChanged();
}

void AppSettings::setAutoConnectAddress(QString val){
    setValue(LABEL_AUTO_CON_ADDR , QVariant(val));
    emit autoConnectAddressChanged();
}

void AppSettings::setLastConnectedDevice(QString address){
    setValue(LABEL_LAST_CONNECTED_DEV , QVariant(address));
    emit lastConnectedDeviceChanged();
}

void AppSettings::save(){
    sync();
}
