#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "serial/bluetoothport.h"
#include "serial/serialport.h"
#include "rlockproxy.h"
#include "appsettings.h"
#include "bluetoothlistener.h"
#include "platform.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QCoreApplication::setOrganizationName("MachInventor");
    QCoreApplication::setOrganizationDomain("www.machinventor.com");
    QCoreApplication::setApplicationName("MachInventor");

    BluetoothPort::registerQmlType();
    SerialPort::registerQmlType();
    SerialComm::registerQmlType();
    Platform::registerQmlType();
    RLockProxy::registerQmlType();
    AppSettings::registerQmlType();
    BluetoothListener::registerQmlType();


    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
