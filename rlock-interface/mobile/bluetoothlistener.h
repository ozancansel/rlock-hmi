#ifndef BLUETOOTHLISTENER_H
#define BLUETOOTHLISTENER_H

#include <QQuickItem>
#include "serial/serialcomm.h"

class BluetoothListener : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(SerialComm* comm READ comm WRITE setComm NOTIFY commChanged)


public:

    static void     registerQmlType();
    BluetoothListener(QQuickItem* parent = nullptr);

    SerialComm*     comm();
    void            setComm(SerialComm* comm);

signals:

    void            commChanged();

private slots:

    void            dataRead();

private:

    SerialComm*     m_comm;
    QString         m_str;

};

#endif // BLUETOOTHLISTENER_H
