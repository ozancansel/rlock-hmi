#ifndef RLOCKPROXY_H
#define RLOCKPROXY_H

#include <QQuickItem>
#include "serial/serialcomm.h"
#include <QRegularExpression>

class RLockProxy : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(SerialComm* comm READ comm WRITE setComm NOTIFY commChanged)
    Q_PROPERTY(int disableTimeout READ disableTimeout NOTIFY disableTimeoutChanged)
    Q_ENUMS(States)

public:

    enum States {   UNKNOWN = 0, LOCKED = 1 , UNLOCKING = 2 , UNLOCKED = 3 , DOOR_CLOSE_TIMEOUT = 4,  LOCKING = 5 ,
                 WRONG_PWD_ENTER = 6 , CHANGE_PWD_WRONG = 7 , CHANGE_PWD_SUCCESS = 8 , INITIAL = 9 };

    static void     registerQmlType();
    RLockProxy(QQuickItem* parent = nullptr);

    SerialComm*     comm();
    void            setComm(SerialComm* comm);
    int             disableTimeout();

public slots:

    States          readState();
    int             login(QString str);

private slots:

    void            decrementTimeout();

signals:

    void            commChanged();
    void            disableTimeoutChanged();

private:

    SerialComm*     m_comm;
    int             m_disableTimeout;
    QTimer          m_timer;

    bool            checkPort();
    QString         recvString(int timeout);
    void            clearPort();
    QRegularExpression  m_expr;

    void            setDisableTimeout(int val);

};

#endif // RLOCKPROXY_H
