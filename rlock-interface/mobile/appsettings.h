#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

#define LABEL_AUTO_CON_ADDR         "autoConnectAddress"
#define LABEL_LAST_CONNECTED_DEV    "lastConnectedDevice"
#define LABEL_AUTO_CONNECT          "autoConnect"

class AppSettings : public QSettings
{

    Q_OBJECT
    Q_PROPERTY(bool autoConnect READ autoConnect WRITE setAutoConnect NOTIFY autoConnectChanged)
    Q_PROPERTY(QString autoConnectAddress READ autoConnectAddress WRITE setAutoConnectAddress NOTIFY autoConnectAddressChanged)
    Q_PROPERTY(QString lastConnectedDevice READ lastConnectedDevice WRITE setLastConnectedDevice NOTIFY lastConnectedDeviceChanged)

public:

    static void     registerQmlType();
    AppSettings(QObject* parent = nullptr);

    bool    autoConnect();
    QString autoConnectAddress();
    QString lastConnectedDevice();
    void    setAutoConnect(bool enabled);
    void    setAutoConnectAddress(QString val);
    void    setLastConnectedDevice(QString address);

public slots:

    void    save();

signals:

    void    autoConnectChanged();
    void    autoConnectAddressChanged();
    void    lastConnectedDeviceChanged();

};

#endif // SETTINGS_H
