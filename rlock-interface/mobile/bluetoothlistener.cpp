#include "bluetoothlistener.h"

void BluetoothListener::registerQmlType(){
    qmlRegisterType<BluetoothListener>("RLock" , 1 , 0 , "BluetoothListener");
}

BluetoothListener::BluetoothListener(QQuickItem* parent)
    :
      QQuickItem(parent)
{

}

SerialComm* BluetoothListener::comm(){
    return m_comm;
}

void BluetoothListener::setComm(SerialComm *comm){
    m_comm = comm;
    connect(comm->device() , SIGNAL(readyRead()) , this , SLOT(dataRead()));
    emit commChanged();
}

void BluetoothListener::dataRead(){

    if(!isEnabled())
        return;

    QIODevice* device =  comm()->device();
    QString temp = device->readAll();

    if(temp.contains("\n")){
        qDebug() << m_str << temp.left(temp.indexOf("\n"));
        m_str = temp.right(temp.length() - temp.indexOf("\n") - 1);
    } else {
        m_str.append(temp);
    }
}
