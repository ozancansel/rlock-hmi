#include "rlockproxy.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QStringRef>
#include <QThread>
#include <QAbstractEventDispatcher>

void RLockProxy::registerQmlType(){
    qmlRegisterType<RLockProxy>("RLock" , 1 , 0 , "RLockProxy");
}

RLockProxy::RLockProxy(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_comm(nullptr) ,
      m_expr("{.([^}]+)*}") ,
      m_disableTimeout(0)
{
    m_timer.setInterval(1000);

    connect(&m_timer , SIGNAL(timeout()) , this , SLOT(decrementTimeout()));
}

SerialComm* RLockProxy::comm(){
    return m_comm;
}

void RLockProxy::setComm(SerialComm *comm){
    m_comm = comm;
    emit commChanged();
}

RLockProxy::States RLockProxy::readState(){

    //Check port
    if(!checkPort())
        return UNKNOWN;

    //Clear port
    clearPort();

    //Read state
    m_comm->send(QString("{S}"));

    QString cmd = recvString(10000);

    if(cmd.isEmpty())
        return UNKNOWN;

    int code = cmd.at(1).toLatin1();

    if(code != 'S')
        return UNKNOWN;

    cmd = cmd.mid(2 , cmd.length() - 3);

    return (States)cmd.toInt();
}

bool RLockProxy::checkPort(){

    if(m_comm == nullptr)
        return false;

    if(m_comm->getState() != SerialComm::Connected)
        return false;

    return true;
}

QString RLockProxy::recvString(int timeout){
    //Check port
    if(!checkPort())
        return "";

    QTime   time;

    time.start();

    int start = time.elapsed();

    QIODevice *dev = m_comm->device();

    QString content;
    QRegularExpressionMatch match;

    while(true) {
        QThread::currentThread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
        if(dev->bytesAvailable()){
            //Read str
            content.append(dev->readAll());
            match = m_expr.match(content);

            //If contains command
            if(match.hasMatch()){
                int start = match.capturedStart(0);
                int end = match.capturedEnd(0);
                QString res = content.mid(start , end - start);
                return res;
            }
        }

        if(time.elapsed() - start > timeout){
            return "";
        }
    }
}

void RLockProxy::clearPort(){
    if(!checkPort())
        return;

    //Clear
    m_comm->device()->readAll();
}

int RLockProxy::disableTimeout(){
    return m_disableTimeout;
}

int RLockProxy::login(QString pwd){

    //Check port
    if(!checkPort())
        return -1;

    clearPort();

    //Read state
    m_comm->send(QString("{L%0}")
                 .arg(pwd));
    QString cmd = recvString(1000);

    if(cmd.isEmpty())
        return -1;

    int code = cmd.at(1).toLatin1();

    if(code != 'L')
        return -1;

    cmd = cmd.mid(2 , cmd.length() - 3);

    QStringList results = cmd.split(",");

    int resCode = QString(results.at(0)).toInt();

    if(resCode == 2){
        setDisableTimeout(QString(results.at(1)).toInt());
        if(m_timer.isActive())
            m_timer.stop();

        m_timer.start();
    }

    qDebug() << results;

    return results[0].toInt();
}

void RLockProxy::setDisableTimeout(int val){
    m_disableTimeout = val;
    emit disableTimeoutChanged();
}

void RLockProxy::decrementTimeout(){
    setDisableTimeout(disableTimeout() - 1000);

    if(disableTimeout() <= 0){
        setDisableTimeout(0);
        m_timer.stop();
    }
}
