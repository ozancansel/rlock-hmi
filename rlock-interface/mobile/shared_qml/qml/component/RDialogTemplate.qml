import QtQuick 2.7
import QtQuick.Controls 2.2
import "../"

Dialog  {

    property string     headerText  :   ""
    property alias      headerFont  :   headerT.font
    property variant    buttonGroup :   []

    id          :   messageDialog
    z           :   2
    modal       :   true
    closePolicy :   Dialog.NoAutoClose

    enter       :   Transition {
        NumberAnimation { property : "opacity"; from : 0.0; to : 1.0 }
    }

    header      :   Item    {

        id                  :   headerItem
        implicitHeight      :   Responsive.getV(185)

        Text {
            id              :   headerT
            text            :   headerText
            x               :   Responsive.getH(80)
            y               :   Responsive.getV(50)
            font.pixelSize  :   Responsive.getV(80)
            color           :   "white"
        }

        Rectangle{
            id              :   bottomLine
            anchors.left    :   parent.left
            anchors.right   :   parent.right
            anchors.bottom  :   parent.bottom
            height          :   Responsive.getV(2)
        }

        RCloseButton{
            id                  :   closeDialog
            width               :   Responsive.getH(110)
            height              :   width
            anchors.right       :   parent.right
            anchors.rightMargin :   Responsive.getH(50)
            anchors.top         :   parent.top
            anchors.topMargin   :   Responsive.getV(40)
            onClicked           :   reject()
        }
    }

    footer      :   DialogButtonBox {
        id              :   dialogBox
        standardButtons :   messageDialog.standardButtons
        position        :   DialogButtonBox.Footer
        alignment       :   Qt.AlignTop | Qt.AlignRight
        spacing         :   Responsive.getH(50)
        implicitHeight  :   Responsive.getV(190)
        anchors.right   :   parent.right
        anchors.rightMargin     :   Responsive.getH(110)

        Repeater{
            model   :   buttonGroup.length

            RButton{
                implicitHeight              :   Responsive.getV(120)
                implicitWidth               :   Responsive.getH(240)
                text                        :   buttonGroup[index].text
                font.pixelSize              :   Responsive.getV(50)
                DialogButtonBox.buttonRole  :   buttonGroup[index].role
            }
        }
        background      :   Item {  }
    }

    background  :   BackgroundPanel {
        height          :   messageDialog.height
        width           :   messageDialog.width
        borderVisible   :   true
    }
}
