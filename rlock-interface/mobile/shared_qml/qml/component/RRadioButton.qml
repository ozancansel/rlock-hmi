import QtQuick 2.6
import QtQuick.Controls 2.1

RadioButton {
    id: control
    text: qsTr("RadioButton")

    indicator: Rectangle {
        width       :   control.height
        height      :   control.height
        radius      :   height / 2
        color       :   "#191919"
        border.color:   "white"
        border.width:   height * 0.07

        Rectangle {
            width   : control.height * 0.65
            height  : control.height * 0.65
            anchors.centerIn    :   parent
            color   : "white"
            visible : control.checked
            radius  :   height / 2
        }
    }

    contentItem: Text {
        text                :   control.text
        font                :   control.font
        opacity             :   enabled ? 1.0 : 0.3
        color               :   "white"
        horizontalAlignment :   Text.AlignHCenter
        verticalAlignment   :   Text.AlignVCenter
        leftPadding         :   control.indicator.width + control.spacing
    }
}
