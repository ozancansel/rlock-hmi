import QtQuick 2.7
import QtQuick.Controls 2.2
import "../"

ToolTip{
    id      :   tooltipControl
    contentItem :   Text {
        text                :   tooltipControl.text
        font                :   tooltipControl.font
        color               :   "white"
        anchors.centerIn    :   tooltipControl.background
        horizontalAlignment :   Text.AlignHCenter
        verticalAlignment   :   Text.AlignVCenter
    }
    background  :   Item{
        width   :   tooltipControl.contentItem.width + Responsive.getH(60)
        height  :   tooltipControl.contentItem.height + Responsive.getV(60)

        Rectangle {
            anchors.fill    :   parent
            color           :   "#191919"
            radius          :   height * 0.2
            border.width    :   Responsive.getV(2)
            border.color    :   "white"
            z               :   2
        }

        Rectangle{
            width           :   Responsive.getH(60)
            height          :   Responsive.getV(60)
            anchors.bottom  :   parent.bottom
            anchors.horizontalCenter    :   parent.horizontalCenter
            rotation        :   45
            color           :   "#191919"
            border.width    :   Responsive.getV(2)
            border.color    :   "white"
        }
    }
}
