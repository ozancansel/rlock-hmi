import QtQuick 2.0
import RLib 1.0
import "../"

Item {

    id                              :   visualizer

    property double from            :   0
    property double to              :   0
    property double pos             :   0.5
    property real   tickCount       :   0
    readonly property real portion  :   Math.abs(from - to) / tickCount
    readonly property int sign      :   from > to ? -1 : 1
    property color  stageColor      :   "green"
    property real   velocity        :   0

    signal draggedTo(real pos);

    function calcPos(pos , item){
        var w = visualizer.width

        return visualizer.width * pos - (item.width / 2)
    }

    function calcStagePos(){
        return Math.min(Math.max(0 , (visualizer.width - stageVisual.width) * pos) , visualizer.width - stageVisual.width)
    }

    clip        :   true

    Rectangle   {
        id      :   stageVisual
        width   :   parent.width * 0.05
        height  :   parent.height - y
        y       :   Responsive.getV(50)
        color   :   stageColor
        z       :   3
        x       :   calcStagePos()
//        Behavior on x {
//            SmoothedAnimation{
//                velocity    :   visualizer.velocity
//            }
//        }
        radius  :   height * 0.3
        border.width    :   Responsive.getV(3)
        border.color    :   "white"

        Drag.active     :   visualizerDragArea.drag.active
    }

    Rectangle{
        id      :   posIndicator
        height  :   parent.height
        width   :   stageVisual.width * 0.12
        anchors.horizontalCenter    :   stageVisual.horizontalCenter
        color   :   stageVisual.color
        z       :   2
        radius  :   width * 0.2
    }


    MouseArea{
        id          :   visualizerDragArea
        anchors.fill:   parent
        drag.target :   stageVisual
        drag.axis   :   Drag.XAxis
        onReleased  :   {
            draggedTo((stageVisual.x + stageVisual.width / 2) / visualizer.width)
            stageVisual.x = Qt.binding(function(){ return calcStagePos()})
        }
    }

    FontLoader  {
        id      :   numericFont
        source  :   "/res/font/digital-7/digital-7.ttf"
    }

    BackgroundPanel{
        anchors.fill    :   parent
    }

    Repeater{
        model   :   tickCount + 1

        Text {
            id              :   tickItem
            text            :   (from + (index * portion * sign)).toFixed(2)
            x               :   calcPos(index / (tickCount + 1) , tickItem) + stageVisual.width / 2
            y               :   Responsive.getV(20)
            font.pixelSize  :   Responsive.getV(30)
            font.family     :   numericFont.name
            color           :   "white"
        }
    }
}
