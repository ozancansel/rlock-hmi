import QtQuick 2.0
import QtQuick.Controls 2.2

Button {

    id          :   control
    property string     source  :   ""

    background  :   Item {

    }

    contentItem :   Item {
        id      :   contentIt
        width   :   control.width
        height  :   control.height

        Image {
            id              :   controlIcon
            source          :   control.source
            fillMode        :   Image.PreserveAspectFit
            smooth          :   true
            anchors.fill    :   parent
            anchors.centerIn:   parent
            anchors.margins :   Math.min(contentIt.width , contentIt.height) * 0.1
        }
    }
}
