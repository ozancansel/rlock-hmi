import QtQuick 2.0
import QtQuick.Controls 2.2
import "../"

Button {
    id  :   button

    background  :   Item{
        width           :   button.width
        height          :   button.height
        Rectangle {
            anchors.fill    :   parent
            color           :   button.pressed ? "#A8A8A8" : "#191919"
            radius          :   height * 0.5
            border.width    :   Responsive.getV(4)
            border.color    :   "white"
        }

        Rectangle{
            width           :   parent.width * 0.6
            height          :   parent.height * 0.1
            radius          :   height * 0.2
            rotation        :   45
            anchors.centerIn:   parent
        }

        Rectangle{
            width           :   parent.width * 0.6
            height          :   parent.height * 0.1
            radius          :   height * 0.2
            rotation        :   -45
            anchors.centerIn:   parent
        }
    }

    contentItem :   Item{

    }
}
