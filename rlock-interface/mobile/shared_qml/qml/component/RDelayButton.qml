import QtQuick 2.0
import QtQuick.Controls 2.2
import "../"

DelayButton {

    id  :   button

    readonly property int   normal  :   0
    readonly property int   error   :   1
    property int buttonType         :   normal
    scale       :   pressed ? 0.96  : 1

    Behavior on scale{
        NumberAnimation{
            duration    :   100
        }
    }

    background  :   Rectangle {
        width           :   button.width
        height          :   button.height
        color           :   {
            if(buttonType === normal){
                return "#191919"
            } else if (buttonType === error)
                return "#b91919"
        }
        radius          :   height * 0.1
        border.width    :   Responsive.getV(4)
        border.color    :   "white"

        Rectangle{
            height      :   parent.height
            width       :   button.progress * button.width
            color       :   {
                if(buttonType === normal)   return "#464646"
                if(buttonType === error)    return "#e43a3a"
            }
            border.width:   parent.border.width
            border.color:   parent.border.color
            radius      :   parent.radius
            visible     :   progress
        }
    }

    contentItem :   Item{
        Text {
            id                  :   name
            text                :   button.text
            anchors.centerIn    :   parent
            font                :   button.font
            color               :   "white"
        }
    }
}
