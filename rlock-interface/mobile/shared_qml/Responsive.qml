pragma Singleton
import QtQuick 2.0
import QtQuick.Window 2.2

Item    {

    property real appWidth          :   Screen.width
    property real appHeight         :   Screen.height
    property real vFactor           :   (appHeight) / 1080
    property real hFactor           :   appWidth / 1920
    property real hPortraitFactor   :   (appWidth) / 1080
    property real vPortraitFactor   :   (appHeight) / 1920

    property int bottomOffset       :   0

    function getV(y){
        return vFactor * y
    }

    function getH(x){
        return hFactor * x
    }

    function pH(x){
        return hPortraitFactor * x
    }

    function pV(y){
        return vPortraitFactor * y
    }
}
