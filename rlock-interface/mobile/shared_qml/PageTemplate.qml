import QtQuick 2.0
import "component"
import "../qml"

Item {

    property string     headerText  :   ""
    property alias      headerFont  :   headerTextItem.font
    property bool       headerVisible   :   true

    Rectangle{
        id          :   mainBackground
        anchors.fill:   parent
        color       :   "#383838"
    }

    BackgroundPanel {
        id          :   headerPanel
        width       :   Responsive.getH(1880)
        height      :   Responsive.getV(150)
        x           :   Responsive.getH(20)
        y           :   Responsive.getV(20)
        visible     :   headerVisible

        Text {
            id                  :   headerTextItem
            text                :   headerText
            font.pixelSize      :   Responsive.getV(50)
            color               :   "white"
            anchors.centerIn    :   parent
            horizontalAlignment :   Text.AlignHCenter
            verticalAlignment   :   Text.AlignVCenter
        }
    }
}
