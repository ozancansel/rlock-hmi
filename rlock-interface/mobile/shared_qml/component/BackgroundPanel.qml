import QtQuick 2.0
import "../"

Rectangle{

    property real   containerRadius     :   0.2
    property bool   borderVisible       :   false
    property bool   pressed             :   false
    property color  pressedColor        :   "#222222"

    id              :   control

    color           :   pressed ? pressedColor : "#191919"

    border.width    :   borderVisible ? Responsive.getV(5) : 0
//    border.color    :   "#ab2800"
    border.color    :   "white"
    radius          :   Math.min(width , height) * containerRadius
}
