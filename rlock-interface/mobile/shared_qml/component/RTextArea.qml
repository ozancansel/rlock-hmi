import QtQuick 2.0
import QtQuick.Controls 2.2
import "../"

TextArea {
    id          :   textInput
    background  :   Item  {
        Rectangle {
            anchors.fill    :   parent
            anchors.margins :   -parent.height * 0.1
            color           :   "white"
            radius          :   Math.min(Responsive.getV(20) , height * 0.2)
        }
    }
}
