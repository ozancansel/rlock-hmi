import QtQuick 2.7
import QtQuick.Controls 2.2
import RLib 1.0
import "../qml"
import "component"

Item {


    property SerialComm comm    :   ({})

    function    startScan(){
        ports.clear()

        comm.startScan()
    }

    Rectangle{
        id          :   mainBackground
        anchors.fill:   parent
        color       :   "#383838"
    }

    BackgroundPanel{
        id          :   headerPanel
        width       :   Responsive.getH(1850)
        height      :   Responsive.getV(150)
        x           :   Responsive.getH(20)
        y           :   Responsive.getV(20)

        Text {
            id              :   headerText
            text            :   "Port Listesi"
            font.pixelSize  :   Responsive.getV(50)
            color           :   "white"
            anchors.centerIn:   parent
        }
    }

    BackgroundPanel{
        id                  :   portNotFoundPanel
        anchors.centerIn    :   parent
        width               :   Responsive.getH(1000)
        height              :   Responsive.getV(600)
        visible             :   ports.count === 0 && comm.state !== SerialComm.Discovering && comm.state !== SerialComm.Connected
        borderVisible       :   true
        Text {
            id              :   portNotFoundText
            text            :   "Port Yok !\nTara butonuna basın..."
            font.pixelSize  :   Responsive.getV(80)
            color           :   "white"
            anchors.centerIn:   parent
            horizontalAlignment :   Text.AlignHCenter
            verticalAlignment   :   Text.AlignVCenter
        }
    }

    BackgroundPanel{
        id                  :   discoveringPanel
        anchors.centerIn    :   parent
        width               :   Responsive.getH(1000)
        height              :   Responsive.getV(600)
        visible             :   ports.count === 0 && comm.state === SerialComm.Discovering
        borderVisible       :   true
        Text {
            id                  :   discoveringText
            text                :   "Taranıyor...\nHenüz cihaz bulunamadı."
            font.pixelSize      :   Responsive.getV(80)
            color               :   "white"
            anchors.centerIn    :   parent
            horizontalAlignment :   Text.AlignHCenter
            verticalAlignment   :   Text.AlignVCenter
        }
    }

    RDelayButton{
        id                  :   disconnectButton
        anchors.centerIn    :   parent
        width               :   Responsive.getH(1000)
        height              :   Responsive.getV(600)
        visible             :   comm.state == SerialComm.Connected
        text                :   "Bağlantıyı Kapat"
        font.pixelSize      :   Responsive.getV(100)
        buttonType          :   normal
        delay               :   700
        onActivated         :   {
            comm.closeSocket()
            progress = 0
        }
    }

    ListModel{
        id                  :   ports
    }

    ListView    {
        id                  :   portList
        anchors.fill        :   parent
        anchors.topMargin   :   Responsive.getV(180)
        anchors.bottomMargin:   Responsive.getV(80)
        model               :   ports
        visible             :   comm.state !== SerialComm.Connected
        delegate            :   Item    {

            id      :   cont
            height  :   Responsive.getV(100)
            width   :   portList.width

            Text {
                id                  :   portText
                text                :   name + " - " + address
                anchors.centerIn    :   parent
                font.pixelSize      :   Responsive.getV(40)
                color               :   "white"
            }

            MouseArea{
                id          :   area
                onClicked   :   portList.currentIndex = index
                anchors.fill:   parent
            }
        }
        highlight                   :   Rectangle{color : "#2286fb"}
    }

    Row {

        anchors.bottom              :   parent.bottom
        anchors.bottomMargin        :   Responsive.getV(40)
        anchors.horizontalCenter    :   parent.horizontalCenter
        spacing                     :   Responsive.getH(50)

        RButton {
            id              :   scanButton
            width           :   Responsive.getH(320)
            height          :   Responsive.getV(150)
            text            :   "Tara"
            font.pixelSize  :   Responsive.getV(50)
            onClicked       :   startScan()
            enabled         :   comm.state != SerialComm.Connected
        }

        SequentialAnimation{

            id              :   scanAnimation
            running         :   comm.state === SerialComm.Discovering
            loops           :   Animation.Infinite
            onStopped       :   scanButton.opacity = 1

            NumberAnimation{
                target      :   scanButton
                property    :   "opacity"
                from        :   1
                to          :   0.6
                duration    :   500
            }


            PauseAnimation {
                duration    : 100
            }

            NumberAnimation{
                target      :   scanButton
                property    :   "opacity"
                from        :   0.6
                to          :   1
                duration    :   500
            }
        }


        RButton {
            id              :   connectButton
            width           :   Responsive.getH(320)
            height          :   Responsive.getV(150)
            text            :   "Bağlan"
            font.pixelSize  :   Responsive.getV(50)
            enabled         :   ports.count > 0 && comm.state != SerialComm.Connected
            onClicked       :   {
                comm.connectTo(ports.get(portList.currentIndex).address)
            }
        }
    }


    Dialog  {
        id          :   connectingDialog
        x           :   parent.width / 2 - (width / 2)
        y           :   parent.height / 2 - (height / 2)
        visible     :   comm.state == SerialComm.Connecting
        width       :   Responsive.getH(800)
        height      :   Responsive.getV(450)
        z           :   2
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        enter       :   Transition {
            NumberAnimation { property : "opacity"; from : 0.0; to : 1.0 }
        }

        contentItem :   BackgroundPanel {
            id              :   panel
            borderVisible   :   true

            Text {
                id                  :   connectingText
                text                :   qsTr("Baglaniliyor...")
                anchors.centerIn    :   parent
                font.pixelSize      :   Responsive.getV(90)
                color               :   "white"
            }

            RCloseButton{
                id              :   closeButton
                width           :   Responsive.getH(70)
                height          :   width
                anchors.right   :   parent.right
                anchors.rightMargin :   panel.radius / 2
                anchors.top     :   parent.top
                anchors.topMargin   :   panel.radius / 2
                onClicked       :   connectingDialog.close()
            }

            SequentialAnimation{
                readonly property int stepDelay :   400

                id          :   connectingAnimation
                loops       :   Animation.Infinite
                running     :   connectingDialog.visible

                ScriptAction{
                    script: connectingText.text = "Bağlanılıyor.  "
                }

                PauseAnimation {
                    duration    :   connectingAnimation.stepDelay
                }

                ScriptAction{
                    script: connectingText.text = "Bağlanılıyor.. "
                }

                PauseAnimation {
                    duration    :   connectingAnimation.stepDelay
                }
                ScriptAction{
                    script: connectingText.text = "Bağlanılıyor..."
                }
                PauseAnimation {
                    duration    :   connectingAnimation.stepDelay
                }
            }
        }
        background  :   Item
        {

        }
    }

    Connections {
        target              :   comm
        onDeviceDiscovered  :   {
            ports.append({ "name" : name , "address" : address})
        }
    }
}
