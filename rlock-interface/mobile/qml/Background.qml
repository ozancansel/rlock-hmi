import QtQuick 2.0
import QtQuick.Window 2.2

Item{
    anchors.fill        :   parent
    clip                :   true

    Image   {
        source          :   Screen.height > 1080 ? "/res/img/background-2k.jpg" : "/res/img/background.png"
        x               :   -((width - parent.width) / 2)
        y               :   -((height - parent.height) / 2)
    }
}
