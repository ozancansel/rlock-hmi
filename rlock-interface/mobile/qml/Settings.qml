import QtQuick 2.0
import QtQuick.Controls 2.2
import RLock 1.0
import "../shared_qml"
import "../shared_qml/component"

Item {

    property AppSettings    settings    :   ({})

    FontLoader  {
        id      :   libertineFont
        source  :   "/res/font/LinLibertine_Re-4.7.5.ttf"
    }

    Column  {
        anchors.horizontalCenter    :   parent.horizontalCenter
        spacing     :   Responsive.pV(40)
        y           :   Responsive.pV(50)
        Item {
            width       :   Responsive.pH(1000)
            height      :   Responsive.pV(250)

            Text {
                text                :   "Ayarlar"
                wrapMode            :   Text.Wrap
                fontSizeMode        :   Text.Fit
                anchors.fill        :   parent
                horizontalAlignment :   Text.AlignHCenter
                verticalAlignment   :   Text.AlignVCenter
                font.family         :   libertineFont.name
                font.pixelSize      :   Responsive.pV(100)
                color               :   "white"
                z                   :   2
            }

            BackgroundPanel{
                anchors.fill    :   parent
                opacity         :   0.85
            }
        }

        Flickable {

            height      :   Responsive.pV(1000)
            width       :   Responsive.pH(1000)
            contentHeight   :   settingsColumn.height
            contentWidth    :   settingsColumn.width

            Column{
                id          :   settingsColumn
                anchors.horizontalCenter    :   parent.horizontalCenter
                spacing                     :   Responsive.pV(40)

                Item    {
                    id      :   autoConnectContainer
                    width   :   Responsive.pH(1000)
                    height  :   Responsive.pV(200)

                    Row{

                        z               :   2
                        anchors.centerIn:   parent

                        Text{
                            width           :   Responsive.pH(620)
                            height          :   autoConnectContainer.height
                            wrapMode        :   Text.Wrap
                            fontSizeMode    :   Text.Fit
                            font.pixelSize  :   Responsive.pV(70)
                            font.family     :   libertineFont.name
                            text            :   "Otomatik Bağlan"
                            color           :   "white"
                            verticalAlignment   :   Text.AlignVCenter
                        }

                        Switch{
                            id              :   autoConnectSwitch
                            width           :   Responsive.pH(300)
                            height          :   autoConnectContainer.height
                            checked         :   settings.autoConnect
                        }
                    }

                    Binding {
                        target          :   settings
                        property        :   "autoConnect"
                        value           :   autoConnectSwitch.checked
                    }

                    BackgroundPanel{
                        anchors.fill    :   parent
                        opacity         :   0.7
                    }
                }
            }
        }
    }

    Text {
        anchors.horizontalCenter    :   parent.horizontalCenter
        y               :   Responsive.pV(1680)
        text            :   qsTr("R&G Steel Doors Inc.\nPowered By Machinventor")
        horizontalAlignment :   Text.AlignHCenter
        color           :   "white"
        z               :   3
        font.pixelSize  :   Responsive.pV(45)
        font.family :   libertineFont.name
    }
}
