import QtQuick 2.0
import QtQuick.Controls 2.2
import RLib 1.0
import RLock 1.0
import "../shared_qml/component"
import "../shared_qml"
import "comp"

Item {


    property RLockProxy    proxy    :   ({})

    id  :   unlockPage

    FontLoader{
        id      :   libertineFont
        source  :   "/res/font/LinLibertine_Re-4.7.5.ttf"
    }

//    RToastMessage{
//        id      :   pwdIsTooShortToast
//        message :   "Şifre 6 karakterden uzun olmalıdır."
//        x       :   parent.width / 2 - width / 2
//        y       :   parent.height / 2 - height / 2
//        width   :   Responsive.pH(1000)
//        height  :   Responsive.pV(660)
//        headerText  :   "Şifre Kısa"
//        messageFont.family  :   libertineFont.name
//        messageFont.pixelSize   :   Responsive.pV(65)
//        background.opacity      :   0.85
//    }

    Column  {

        spacing             :   Responsive.pV(50)
        anchors.centerIn    :   parent
        opacity             :   !disableAnimation.visible

        Text {
            id              :   header
            text            :   qsTr("R&G")
            font.pixelSize  :   Responsive.pV(160)
            font.family     :   libertineFont.name
            color           :   "#eaedca"
            anchors.horizontalCenter    :   parent.horizontalCenter
            y               :   Responsive.pV(40)
        }

        Item    {
            width   :   numPadGrid.width
            height  :   Responsive.pV(300)

            BackgroundPanel{
                id              :   pwdPanel
                anchors.fill    :   parent
                opacity         :   0.85
                borderVisible   :   true
            }

            RIconButton {
                id      :   lockIconImg
                source  :   "/res/img/lock.png"
                width   :   Responsive.pH(250)
                height  :   Responsive.pV(250)
                anchors.verticalCenter  :   parent.verticalCenter
                x       :   Responsive.pH(30)
                smooth  :   true
            }

            TextField {
                id                  :   passwordField
                text                :   ""
                anchors.verticalCenter  :   parent.verticalCenter
                inputMethodHints    :   Qt.ImhHiddenText
                anchors.right       :   parent.right
                anchors.rightMargin :   Responsive.pH(40)
                anchors.left        :   lockIconImg.right
                anchors.leftMargin  :   Responsive.pH(-60)
                echoMode            :   TextField.Password
                color               :   "white"
                font.family         :   libertineFont.name
                font.pixelSize      :   passwordField.width / (maximumLength * 0.7)
                background          :   Item { }
                width               :   Responsive.pH(700)
                horizontalAlignment :   Text.AlignHCenter
                verticalAlignment   :   Text.AlignVCenter
                readOnly            :   true
                maximumLength       :   12
            }
        }

        Grid    {
            id      :   numPadGrid
            columnSpacing   :   Responsive.pH(30)
            rowSpacing      :   Responsive.pV(40)
            rows    :   4
            columns :   3
            anchors.horizontalCenter    :   parent.horizontalCenter

            Repeater    {
                model       :   9
                NumButton   {
                    width   :   Responsive.pH(300)
                    height  :   Responsive.pV(250)
                    text    :   {
                        return index + 1
                    }
                    font.family         :   libertineFont.name
                    font.pixelSize      :   Responsive.pV(150)
                    onClicked           :   passwordField.text += index + 1
                }
            }

            RIconButton {
                id      :   removeButton
                width   :   Responsive.pH(300)
                height  :   Responsive.pV(250)
                source  :   "/res/img/del-icon.png"
                onClicked   :   passwordField.text = ""
                background  :   BackgroundPanel {
                    width           :   removeButton.width
                    height          :   removeButton.height
                    border.width    :   Responsive.pV(5)
                    opacity         :   0.8
                    pressed         :   removeButton.pressed
                    pressedColor    :   "#860403"
                }
            }

            NumButton{
                width   :   Responsive.pH(300)
                height  :   Responsive.pV(250)
                text    :   "0"
                font.family         :   libertineFont.name
                font.pixelSize      :   Responsive.pV(150)
                onClicked           :   passwordField.text += "0"
            }

            RIconButton {
                id      :   enterButton
                width   :   Responsive.pH(300)
                height  :   Responsive.pV(250)
                source  :   "/res/img/enter-icon.png"
                background  :   BackgroundPanel {
                    width           :   enterButton.width
                    height          :   enterButton.height
                    border.width    :   Responsive.pV(5)
                    opacity         :   0.8
                    pressed         :   enterButton.pressed
                    pressedColor    :   "#003909"
                }
                onClicked:  {


                    if(passwordField.text.length < 6){
                        unlockPwdWrongAnim.start()
                        return;
                    }

                    if(proxy.readState() !== RLockProxy.LOCKED){
                        return;
                    }

                    var res = proxy.login(passwordField.text)

                    if(res === 0 || res === -1){
                        unlockPwdWrongAnim.start()
                    } else if(res === 1){
                        unlockSuccessAnim.start()
                    }

                    passwordField.text = ""
                }
            }
        }
    }

    SequentialAnimation {
        id      :   unlockSuccessAnim


        ScriptAction{
            script  :   pwdPanel.color = "#007331"
        }

        PauseAnimation {
            duration: 150
        }

        ScriptAction{
            script  :   pwdPanel.color = "#191919"
        }

        PauseAnimation {
            duration: 100
        }

        ScriptAction{
            script  :   pwdPanel.color = "#007331"
        }

        PauseAnimation {
            duration: 350
        }

        ColorAnimation {
            to    :   "#191919"
            from      :   "#007331"
            duration:   400
            target  :   pwdPanel
            property:   "color"
        }
    }

    SequentialAnimation {
        id      :   unlockPwdWrongAnim


        ScriptAction{
            script  :   {
                pwdPanel.color = "#a5120d"
                lockIconImg.source = "/res/img/fail-lock.png"
            }
        }

        PauseAnimation {
            duration: 150
        }

        ScriptAction{
            script  :   pwdPanel.color = "#191919"
        }

        PauseAnimation {
            duration: 100
        }

        ScriptAction{
            script  :   pwdPanel.color = "#a5120d"
        }

        PauseAnimation {
            duration: 350
        }

        ColorAnimation {
            to      :   "#191919"
            from    :   "#a5120d"
            duration:   400
            target  :   pwdPanel
            property:   "color"
        }

        ScriptAction{
            script  :   lockIconImg.source = "/res/img/lock.png"
        }
    }

    Dialog  {
        id      :   disableAnimation
        width   :   unlockPage.width
        height  :   unlockPage.height
        visible :   proxy.disableTimeout > 0
        closePolicy :   Dialog.NoAutoClose

        enter       :   Transition {
            NumberAnimation { property : "opacity"; from : 0.0; to : 1.0; duration : 600 }
        }

        exit        :   Transition {
            NumberAnimation { property : "opacity"; from : 1.0; to : 0.0; duration  :   1000 }
        }

        contentItem :   Item {
            Column{
                spacing                 :   Responsive.pV(100)
                anchors.centerIn        :   parent
                Text    {
                    font.pixelSize      :   Responsive.pV(130)
                    font.family         :   libertineFont.name
                    text                :   "Güvenlik Sebebiyle\nSistem Kilitli"
                    color               :   "white"
                    fontSizeMode        :   Text.Fit
                    wrapMode            :   Text.Wrap
                    width               :   unlockPage.width
                    horizontalAlignment:    Text.AlignHCenter
                    verticalAlignment   :   Text.AlignVCenter
                }

                Text    {
                    font.pixelSize      :   Responsive.pV(500)
                    font.family         :   libertineFont.name
                    text                :   (proxy.disableTimeout / 1000).toFixed(0)
                    color               :   "white"
                    fontSizeMode        :   Text.Fit
                    wrapMode            :   Text.Wrap
                    width               :   unlockPage.width
                    horizontalAlignment:    Text.AlignHCenter
                    verticalAlignment   :   Text.AlignVCenter
                }
            }
        }

        background  :   Item {
            Rectangle{
                anchors.fill    :   parent
                color   :   "red"
                opacity :   0.2
            }
        }

        header      :   Item{

        }
    }
}
