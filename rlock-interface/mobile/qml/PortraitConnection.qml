import QtQuick 2.7
import QtQuick.Controls 2.2
import RLib 1.0
import RLock 1.0
import "../shared_qml"
import "../shared_qml/component"

Item {


    property SerialComm comm        :   ({})
    property AppSettings settings   :   ({})
    property bool isCurrentPage     :   false
    property bool firstConnection   :   true
    property bool firstScan         :   true

    function    startScan(){
        ports.clear()

        comm.startScan()
    }

    onIsCurrentPageChanged  :   {
        if(isCurrentPage && comm.state !== SerialComm.Connected){
            if(firstScan){
                startScan()
                firstScan = false
            }
        }
    }

    FontLoader{
        id      :   libertineFont
        source  :   "/res/font/LinLibertine_Re-4.7.5.ttf"
    }

    Item{
        width       :   Responsive.pH(1000)
        height      :   Responsive.pV(250)
        anchors.horizontalCenter    :   parent.horizontalCenter
        y           :   Responsive.pV(40)

        BackgroundPanel{
            id          :   headerPanel
            anchors.fill:   parent
            opacity     :   0.8
        }

        Text {
            id              :   headerText
            text            :   "Kapı Listesi"
            font.pixelSize  :   Responsive.pV(100)
            font.family     :   libertineFont.name
            color           :   "white"
            anchors.centerIn:   parent
        }
    }

    Item    {
        id                  :   portNotFoundPanel
        anchors.centerIn    :   parent
        width               :   Responsive.pH(1000)
        height              :   Responsive.pV(600)
        visible             :   ports.count === 0 && comm.state !== SerialComm.Discovering && comm.state !== SerialComm.Connected

        BackgroundPanel{
            borderVisible       :   true
            anchors.fill        :   parent
            opacity             :   0.8
        }

        Text {
            id              :   portNotFoundText
            text            :   "Port Yok !\nTara butonuna basın..."
            font.family     :   libertineFont.name
            font.pixelSize  :   Responsive.pV(100)
            width           :   parent.width - Responsive.getH(50)
            height          :   parent.height
            verticalAlignment   :   Text.AlignVCenter
            horizontalAlignment :   Text.AlignHCenter
            wrapMode        :   Text.Wrap
            fontSizeMode        :   Text.Fit
            color           :   "white"
            anchors.centerIn:   parent
        }
    }

    BackgroundPanel {
        id                  :   discoveringPanel
        anchors.centerIn    :   parent
        width               :   Responsive.pH(1000)
        height              :   Responsive.pV(600)
        visible             :   ports.count === 0 && comm.state === SerialComm.Discovering
        borderVisible       :   true
        Text {
            id                  :   discoveringText
            text                :   "Taranıyor...\nHenüz cihaz bulunamadı."
            font.pixelSize      :   Responsive.pV(80)
            font.family         :   libertineFont.name
            color               :   "white"
            anchors.centerIn    :   parent
            horizontalAlignment :   Text.AlignHCenter
            verticalAlignment   :   Text.AlignVCenter
        }
    }

    RDelayButton{
        id                  :   disconnectButton
        anchors.centerIn    :   parent
        width               :   Responsive.pH(1000)
        height              :   Responsive.pV(600)
        visible             :   comm.state === SerialComm.Connected
        text                :   "Bağlantıyı Kapat"
        font.pixelSize      :   Responsive.pV(120)
        font.family         :   libertineFont.name
        buttonType          :   normal
        delay               :   700
        onActivated         :   {
            comm.closeSocket()
            progress = 0
        }
    }

    ListModel{
        id                  :   ports
    }

    ListView    {
        id                  :   portList
        anchors.fill        :   parent
        anchors.topMargin   :   Responsive.pV(320)
        anchors.bottomMargin:   Responsive.pV(80)
        model               :   ports
        visible             :   comm.state !== SerialComm.Connected
        delegate            :   Item    {

            id      :   cont
            height  :   Responsive.pV(250)
            width   :   portList.width

            Text {
                id                  :   portText
                text                :   {
                    var str;
                    if(name === "")
                        str = name + " - " + address
                    else
                        str = name
                }
                anchors.fill        :   parent
                anchors.leftMargin  :   Responsive.pV(30)
                anchors.rightMargin :   Responsive.pV(30)
                anchors.topMargin   :   Responsive.pV(50)
                anchors.bottomMargin:   Responsive.pV(50)
                horizontalAlignment :   Text.AlignHCenter
                verticalAlignment   :   Text.AlignVCenter
                font.pixelSize      :   Responsive.pV(100)
                font.family         :   libertineFont.name
                color               :   settings.autoConnectAddress === address && settings.autoConnect ? "orange" : "white"
                fontSizeMode        :   Text.Fit
                wrapMode            :   Text.Wrap
            }

            MouseArea   {
                id              :   area
                onClicked       :   portList.currentIndex = index
                anchors.fill    :   parent
                onPressAndHold  :   {
                    settings.autoConnectAddress = address
                }
            }
        }
        highlight                   :   Rectangle   {
            color   : "#0090fe"
            opacity :   0.65
        }
    }

    Row {

        anchors.bottom              :   parent.bottom
        anchors.bottomMargin        :   Responsive.pV(40)
        anchors.horizontalCenter    :   parent.horizontalCenter
        spacing                     :   Responsive.pH(50)

        RButton {
            id              :   scanButton
            width           :   Responsive.pH(450)
            height          :   Responsive.pV(200)
            text            :   "Tara"
            font.pixelSize  :   Responsive.pV(85)
            onClicked       :   startScan()
            enabled         :   comm.state != SerialComm.Connected
            font.family     :   libertineFont.name
            background.opacity  :   0.8
        }

        SequentialAnimation{

            id              :   scanAnimation
            running         :   comm.state === SerialComm.Discovering
            loops           :   Animation.Infinite
            onStopped       :   scanButton.opacity = 1

            NumberAnimation{
                target      :   scanButton
                property    :   "opacity"
                from        :   1
                to          :   0.6
                duration    :   500
            }


            PauseAnimation {
                duration    : 100
            }

            NumberAnimation{
                target      :   scanButton
                property    :   "opacity"
                from        :   0.6
                to          :   1
                duration    :   500
            }
        }


        RButton {
            id              :   connectButton
            width           :   Responsive.pH(450)
            height          :   Responsive.pV(200)
            text            :   "Bağlan"
            font.pixelSize  :   Responsive.pV(85)
            enabled         :   ports.count > 0 && comm.state != SerialComm.Connected
            onClicked       :   {
                comm.connectTo(ports.get(portList.currentIndex).address)
            }
            font.family     :   libertineFont.name
            background.opacity  :   0.8
        }
    }


    Dialog  {
        id          :   connectingDialog
        x           :   parent.width / 2 - (width / 2)
        y           :   parent.height / 2 - (height / 2)
        visible     :   comm.state == SerialComm.Connecting
        width       :   Responsive.pH(1000)
        height      :   Responsive.pV(800)
        z           :   2
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        enter       :   Transition {
            NumberAnimation { property : "opacity"; from : 0.0; to : 1.0 }
        }

        contentItem :   BackgroundPanel {
            id              :   panel
            borderVisible   :   true

            Text {
                id                  :   connectingText
                text                :   qsTr("Bağlanılıyor...")
                anchors.centerIn    :   parent
                font.pixelSize      :   Responsive.pV(120)
                font.family         :   libertineFont.name
                color               :   "white"
            }

            RCloseButton{
                id              :   closeButton
                width           :   Responsive.pH(70)
                height          :   width
                anchors.right   :   parent.right
                anchors.rightMargin :   panel.radius / 2
                anchors.top     :   parent.top
                anchors.topMargin   :   panel.radius / 2
                onClicked       :   connectingDialog.close()
            }

            SequentialAnimation{
                readonly property int stepDelay :   400

                id          :   connectingAnimation
                loops       :   Animation.Infinite
                running     :   connectingDialog.visible

                ScriptAction{
                    script: connectingText.text = "Bağlanılıyor.  "
                }

                PauseAnimation {
                    duration    :   connectingAnimation.stepDelay
                }

                ScriptAction{
                    script: connectingText.text = "Bağlanılıyor.. "
                }

                PauseAnimation {
                    duration    :   connectingAnimation.stepDelay
                }
                ScriptAction{
                    script: connectingText.text = "Bağlanılıyor..."
                }
                PauseAnimation {
                    duration    :   connectingAnimation.stepDelay
                }
            }
        }
        background  :   Item{   }
    }

    Connections {
        target              :   comm
        onDeviceDiscovered  :   {
            ports.append({ "name" : name , "address" : address})

            if(settings.autoConnect && address == settings.autoConnectAddress && firstConnection){
                comm.connectTo(settings.autoConnectAddress)
            }
        }
        onStateChanged      :   {
            if(comm.state == SerialComm.Connected){
                //Last connected device is set
                settings.lastConnectedDevice = comm.connectingDevice.address
                firstConnection = false
            }
        }
    }
}
