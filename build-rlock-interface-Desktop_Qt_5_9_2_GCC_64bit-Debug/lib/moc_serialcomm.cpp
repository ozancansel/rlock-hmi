/****************************************************************************
** Meta object code from reading C++ file 'serialcomm.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../rlock-interface/lib/serial/serialcomm.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'serialcomm.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SerialComm_t {
    QByteArrayData data[42];
    char stringdata0[480];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SerialComm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SerialComm_t qt_meta_stringdata_SerialComm = {
    {
QT_MOC_LITERAL(0, 0, 10), // "SerialComm"
QT_MOC_LITERAL(1, 11, 19), // "debugEnabledChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 15), // "baudRateChanged"
QT_MOC_LITERAL(4, 48, 16), // "deviceDiscovered"
QT_MOC_LITERAL(5, 65, 4), // "name"
QT_MOC_LITERAL(6, 70, 7), // "address"
QT_MOC_LITERAL(7, 78, 23), // "connectionStatusChanged"
QT_MOC_LITERAL(8, 102, 23), // "connectingStatusChanged"
QT_MOC_LITERAL(9, 126, 17), // "scanStatusChanged"
QT_MOC_LITERAL(10, 144, 10), // "dataIncome"
QT_MOC_LITERAL(11, 155, 4), // "data"
QT_MOC_LITERAL(12, 160, 12), // "stateChanged"
QT_MOC_LITERAL(13, 173, 23), // "connectingDeviceChanged"
QT_MOC_LITERAL(14, 197, 11), // "scanStarted"
QT_MOC_LITERAL(15, 209, 12), // "scanFinished"
QT_MOC_LITERAL(16, 222, 9), // "connectTo"
QT_MOC_LITERAL(17, 232, 11), // "closeSocket"
QT_MOC_LITERAL(18, 244, 9), // "startScan"
QT_MOC_LITERAL(19, 254, 4), // "send"
QT_MOC_LITERAL(20, 259, 4), // "text"
QT_MOC_LITERAL(21, 264, 5), // "write"
QT_MOC_LITERAL(22, 270, 5), // "array"
QT_MOC_LITERAL(23, 276, 6), // "device"
QT_MOC_LITERAL(24, 283, 10), // "QIODevice*"
QT_MOC_LITERAL(25, 294, 11), // "clearBuffer"
QT_MOC_LITERAL(26, 306, 13), // "sendTimerTick"
QT_MOC_LITERAL(27, 320, 5), // "state"
QT_MOC_LITERAL(28, 326, 15), // "ConnectionState"
QT_MOC_LITERAL(29, 342, 22), // "bufferedCommandsLength"
QT_MOC_LITERAL(30, 365, 16), // "connectingDevice"
QT_MOC_LITERAL(31, 382, 12), // "debugEnabled"
QT_MOC_LITERAL(32, 395, 4), // "baud"
QT_MOC_LITERAL(33, 400, 8), // "BaudRate"
QT_MOC_LITERAL(34, 409, 12), // "Disconnected"
QT_MOC_LITERAL(35, 422, 10), // "Connecting"
QT_MOC_LITERAL(36, 433, 9), // "Connected"
QT_MOC_LITERAL(37, 443, 11), // "Discovering"
QT_MOC_LITERAL(38, 455, 5), // "Error"
QT_MOC_LITERAL(39, 461, 4), // "None"
QT_MOC_LITERAL(40, 466, 7), // "B115200"
QT_MOC_LITERAL(41, 474, 5) // "B9600"

    },
    "SerialComm\0debugEnabledChanged\0\0"
    "baudRateChanged\0deviceDiscovered\0name\0"
    "address\0connectionStatusChanged\0"
    "connectingStatusChanged\0scanStatusChanged\0"
    "dataIncome\0data\0stateChanged\0"
    "connectingDeviceChanged\0scanStarted\0"
    "scanFinished\0connectTo\0closeSocket\0"
    "startScan\0send\0text\0write\0array\0device\0"
    "QIODevice*\0clearBuffer\0sendTimerTick\0"
    "state\0ConnectionState\0bufferedCommandsLength\0"
    "connectingDevice\0debugEnabled\0baud\0"
    "BaudRate\0Disconnected\0Connecting\0"
    "Connected\0Discovering\0Error\0None\0"
    "B115200\0B9600"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SerialComm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       5,  140, // properties
       2,  160, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x06 /* Public */,
       3,    0,  110,    2, 0x06 /* Public */,
       4,    2,  111,    2, 0x06 /* Public */,
       7,    0,  116,    2, 0x06 /* Public */,
       8,    0,  117,    2, 0x06 /* Public */,
       9,    0,  118,    2, 0x06 /* Public */,
      10,    1,  119,    2, 0x06 /* Public */,
      12,    0,  122,    2, 0x06 /* Public */,
      13,    0,  123,    2, 0x06 /* Public */,
      14,    0,  124,    2, 0x06 /* Public */,
      15,    0,  125,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      16,    1,  126,    2, 0x0a /* Public */,
      17,    0,  129,    2, 0x0a /* Public */,
      18,    0,  130,    2, 0x0a /* Public */,
      19,    1,  131,    2, 0x0a /* Public */,
      21,    1,  134,    2, 0x0a /* Public */,
      23,    0,  137,    2, 0x0a /* Public */,
      25,    0,  138,    2, 0x0a /* Public */,
      26,    0,  139,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QByteArray,   22,
    0x80000000 | 24,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      27, 0x80000000 | 28, 0x00495009,
      29, QMetaType::Int, 0x00095001,
      30, QMetaType::QVariantMap, 0x00495001,
      31, QMetaType::Bool, 0x00495103,
      32, 0x80000000 | 33, 0x0049500b,

 // properties: notify_signal_id
       7,
       0,
       8,
       0,
       1,

 // enums: name, flags, count, data
      28, 0x0,    6,  168,
      33, 0x0,    2,  180,

 // enum data: key, value
      34, uint(SerialComm::Disconnected),
      35, uint(SerialComm::Connecting),
      36, uint(SerialComm::Connected),
      37, uint(SerialComm::Discovering),
      38, uint(SerialComm::Error),
      39, uint(SerialComm::None),
      40, uint(SerialComm::B115200),
      41, uint(SerialComm::B9600),

       0        // eod
};

void SerialComm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SerialComm *_t = static_cast<SerialComm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->debugEnabledChanged(); break;
        case 1: _t->baudRateChanged(); break;
        case 2: _t->deviceDiscovered((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->connectionStatusChanged(); break;
        case 4: _t->connectingStatusChanged(); break;
        case 5: _t->scanStatusChanged(); break;
        case 6: _t->dataIncome((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->stateChanged(); break;
        case 8: _t->connectingDeviceChanged(); break;
        case 9: _t->scanStarted(); break;
        case 10: _t->scanFinished(); break;
        case 11: _t->connectTo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->closeSocket(); break;
        case 13: _t->startScan(); break;
        case 14: _t->send((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 15: _t->write((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 16: { QIODevice* _r = _t->device();
            if (_a[0]) *reinterpret_cast< QIODevice**>(_a[0]) = std::move(_r); }  break;
        case 17: _t->clearBuffer(); break;
        case 18: _t->sendTimerTick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::debugEnabledChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::baudRateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::deviceDiscovered)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::connectionStatusChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::connectingStatusChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::scanStatusChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::dataIncome)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::stateChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::connectingDeviceChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::scanStarted)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (SerialComm::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialComm::scanFinished)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        SerialComm *_t = static_cast<SerialComm *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< ConnectionState*>(_v) = _t->getState(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->getBufferedCommandsLength(); break;
        case 2: *reinterpret_cast< QVariantMap*>(_v) = _t->connectingDevice(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->debugEnabled(); break;
        case 4: *reinterpret_cast< BaudRate*>(_v) = _t->baudRate(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        SerialComm *_t = static_cast<SerialComm *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 3: _t->setDebugEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setBaudRate(*reinterpret_cast< BaudRate*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject SerialComm::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_SerialComm.data,
      qt_meta_data_SerialComm,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SerialComm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SerialComm::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SerialComm.stringdata0))
        return static_cast<void*>(this);
    return QQuickItem::qt_metacast(_clname);
}

int SerialComm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void SerialComm::debugEnabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void SerialComm::baudRateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void SerialComm::deviceDiscovered(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void SerialComm::connectionStatusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void SerialComm::connectingStatusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void SerialComm::scanStatusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void SerialComm::dataIncome(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void SerialComm::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void SerialComm::connectingDeviceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void SerialComm::scanStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void SerialComm::scanFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
