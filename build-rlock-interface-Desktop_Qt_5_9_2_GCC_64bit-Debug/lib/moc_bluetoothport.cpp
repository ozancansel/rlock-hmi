/****************************************************************************
** Meta object code from reading C++ file 'bluetoothport.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../rlock-interface/lib/serial/bluetoothport.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'bluetoothport.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_BluetoothPort_t {
    QByteArrayData data[13];
    char stringdata0[176];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BluetoothPort_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BluetoothPort_t qt_meta_stringdata_BluetoothPort = {
    {
QT_MOC_LITERAL(0, 0, 13), // "BluetoothPort"
QT_MOC_LITERAL(1, 14, 9), // "connectTo"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 7), // "address"
QT_MOC_LITERAL(4, 33, 9), // "startScan"
QT_MOC_LITERAL(5, 43, 11), // "btConnected"
QT_MOC_LITERAL(6, 55, 14), // "btDisconnected"
QT_MOC_LITERAL(7, 70, 18), // "btDeviceDiscovered"
QT_MOC_LITERAL(8, 89, 20), // "QBluetoothDeviceInfo"
QT_MOC_LITERAL(9, 110, 14), // "btScanFinished"
QT_MOC_LITERAL(10, 125, 14), // "btStateChanged"
QT_MOC_LITERAL(11, 140, 29), // "QBluetoothSocket::SocketState"
QT_MOC_LITERAL(12, 170, 5) // "state"

    },
    "BluetoothPort\0connectTo\0\0address\0"
    "startScan\0btConnected\0btDisconnected\0"
    "btDeviceDiscovered\0QBluetoothDeviceInfo\0"
    "btScanFinished\0btStateChanged\0"
    "QBluetoothSocket::SocketState\0state"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BluetoothPort[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x0a /* Public */,
       4,    0,   52,    2, 0x0a /* Public */,
       5,    0,   53,    2, 0x08 /* Private */,
       6,    0,   54,    2, 0x08 /* Private */,
       7,    1,   55,    2, 0x08 /* Private */,
       9,    0,   58,    2, 0x08 /* Private */,
      10,    1,   59,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,

       0        // eod
};

void BluetoothPort::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BluetoothPort *_t = static_cast<BluetoothPort *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connectTo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->startScan(); break;
        case 2: _t->btConnected(); break;
        case 3: _t->btDisconnected(); break;
        case 4: _t->btDeviceDiscovered((*reinterpret_cast< const QBluetoothDeviceInfo(*)>(_a[1]))); break;
        case 5: _t->btScanFinished(); break;
        case 6: _t->btStateChanged((*reinterpret_cast< QBluetoothSocket::SocketState(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QBluetoothDeviceInfo >(); break;
            }
            break;
        }
    }
}

const QMetaObject BluetoothPort::staticMetaObject = {
    { &SerialComm::staticMetaObject, qt_meta_stringdata_BluetoothPort.data,
      qt_meta_data_BluetoothPort,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *BluetoothPort::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BluetoothPort::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_BluetoothPort.stringdata0))
        return static_cast<void*>(this);
    return SerialComm::qt_metacast(_clname);
}

int BluetoothPort::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = SerialComm::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
