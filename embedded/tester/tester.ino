#include "config.h"
#include "common.h"
#include "motor.h"
#include "lock.h"

bool targetLocked = false;

void setup(){
  Serial.begin(115200);
  //Init motor
  motorInit();
  lockInit();
  pinMode(A0 , INPUT_PULLUP);
  pinMode(A1 , INPUT_PULLUP);
  pinMode(A2 , INPUT_PULLUP);
}

void loop(){
  checkSystem();
  delay(10);
//motorDir(true);/
//motorEnable(true);/
}

void checkSystem(){
  if(targetLocked && !isLocked()){
    motorDir(true);
    motorEnable(true);
    Serial.println("Locking");
    while(true){
      if(isLocked())
        break;
    }
  } else if(!targetLocked && !isUnlocked()){
    motorDir(false);
    motorEnable(true);
    Serial.println("Unlocking");
     while(true){
      if(isUnlocked())
        break;
    }
  } else {
    Serial.println("Done");
    motorEnable(false);
    delay(random(1000) * 10);
    targetLocked = !targetLocked;
  }
}

