#ifndef DOOR_H
#define DOOR_H

void doorInit();
bool doorIsClosed();

void doorInit(){
  pinMode(DOOR_CLOSED_SWITCH , INPUT);
}

bool doorIsClosed(){
  return analogToDigital(analogRead(DOOR_CLOSED_SWITCH)) == DOOR_CLOSE_LOGIC_TRUE;
}

#endif
