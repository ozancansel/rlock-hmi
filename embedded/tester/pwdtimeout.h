#ifndef PWD_TIMEOUT_H
#define PWD_TIMEOUT_H

uint64_t  attempts[PWD_ATTEMPT_MAX];
uint8_t attempt_idx;

void pwdTimeoutResetCb();

void pwd_timeout_init(){
  //Set all zero
  for(uint8_t i = 0; i < PWD_ATTEMPT_MAX; i++){
    attempts[i] = 0;
  }

  //Set idx 0
  attempt_idx = 0;
}

void wrong_attempt(){
  attempts[attempt_idx] = millis();
  attempt_idx++;
}

uint32_t is_blocked(){
  if(attempt_idx == 0)
    return 0;
  if(attempt_idx > 0){
    if(millis() - attempts[0] > PWD_ATTEMPT_TIMEOUT){
      attempt_idx = 0;
      return 0;
    } else if(attempt_idx >= PWD_ATTEMPT_MAX){
      return PWD_ATTEMPT_TIMEOUT - (millis() - attempts[0]);
    } else {
      return 0;
    }
  }
}

#endif
