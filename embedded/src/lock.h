#ifndef LOCK_H
#define LOCK_H

void lockInit();
bool isLocked();
bool isUnlocked();
bool atMiddle();

void lockInit(){
  pinMode(DOOR_LOCKED_SWITCH , INPUT);
  pinMode(DOOR_UNLOCKED_SWITCH , INPUT);
}

bool isLocked(){
  return analogToDigital(analogRead(DOOR_LOCKED_SWITCH)) == LOCK_LOGIC_TRUE;
}

bool isUnlocked(){
  return analogToDigital(analogRead(DOOR_UNLOCKED_SWITCH)) == UNLOCK_LOGIC_TRUE;
}

bool atMiddle(){
  return !isLocked() && !isUnlocked();
}

#endif
