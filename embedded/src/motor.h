#ifndef MOTOR_H
#define MOTOR_H

void motorInit();
void motorDir(bool cw);
void motorEnable(bool enabled);

void motorInit(){
  pinMode(MOTOR_IN1 , OUTPUT);
  pinMode(MOTOR_IN2 , OUTPUT);
  pinMode(MOTOR_PWM , OUTPUT);
}

void motorDir(bool cw){
  digitalWrite(MOTOR_IN1 , cw);
  digitalWrite(MOTOR_IN2 , !cw);
}

void motorEnable(bool enabled){
  digitalWrite(MOTOR_PWM , enabled);
}

#endif
