#ifndef STORAGE_H
#define STORAGE_H

#define PWD_START_CHAR   115  //'s'
#define PWD_END_CHAR     83   //'S'

bool checkStartIdx();
void eepromInit();
void savePassword(String pwd);
void printFirst(int num){
  for(int i = 0; i < num; i++){
    Serial.write(EEPROM.read(i));
  }
}

void storageInit(){
  int ch = EEPROM.read(0);
  if(ch != PWD_START_CHAR){
    for (int i = 0 ; i < EEPROM.length() ; i++) {
      EEPROM.write(i, 0);
    }
    EEPROM.write(0 , PWD_START_CHAR);
    savePassword("123456");
  }
}

String    retrievePassword(){
  String password = "";
  int idx = 1;
  //Assign not PWD_END_CHAR
  while(idx < MAX_PASSWORD_LENGTH + 2){
    int b = EEPROM.read(idx);
    if(b == PWD_END_CHAR)
      break;
    password += (char)b;
    idx++;
  }
  if(idx >= MAX_PASSWORD_LENGTH + 3)
    return password;
  return password;
}

void    savePassword(String pwd){
  for(int i = 0; i < pwd.length(); i++){
      EEPROM.write(i + 1 , pwd.charAt(i)); 
  } 
  EEPROM.write(pwd.length() + 1, PWD_END_CHAR);
}

bool checkStartIdx(){
  //EEPROM first byte should be equal to PWD_START_CHAR
  uint8_t val = EEPROM.read(0);

  return val == PWD_START_CHAR;
}

int getEndIdx(){
  //Traverse EEPROM
  for(int i = 0; i < EEPROM.length(); i++){
      uint8_t currVal = EEPROM.read(i);
      //If end char is found
      if(currVal == PWD_END_CHAR)
        return i; //Return idx
  }

  //If program reachs here then return minus one which means couldn't found
  return -1;
}

#endif
