#include <Nextion.h>
#include <EEPROM.h>
#include <TaskScheduler.h>
#include "Fsm.h"
#include <SimpleSerial.h>
#include <AESLib.h>
#include "common.h"
#include "config.h"
#include "door.h"
#include "lock.h"
#include "motor.h"
#include "storage.h"
#include "pwdtimeout.h"

SoftwareSerial btSerial(9 , 10);

//#define STATE_DEBUGGING
#define dbgSerial                       Serialiy
#define HOST_SERIAL                     btSerial
#define CMD_LOGIN                       76  //'L'
#define CMD_GET_STATE                   83  //'S'
#define CMD_GET_VERSION                 71  //'G'
#define CMD_ENTER_ADMIN_MODE_W_PWD      65  //'A , <Key>'
#define CMD_MOVE_MOTOR_FOR_UNLOCK       62  //'>'
#define CMD_MOVE_MOTOR_FOR_LOCK         60  //'<'
#define CMD_STOP_MOTOR                  66  //'B'
#define CMD_EXIT_ADMIN_MODE             69  //'E'

#define TR_DOOR_CLOSE                   0
#define TR_DOOR_OPEN                    1
#define TR_LOCK_SWITCH_TRUE             2
#define TR_LOCK_SWITCH_FALSE            3
#define TR_UNLOCK_SWITCH_TRUE           4
#define TR_UNLOCK_SWITCH_FALSE          5
#define TR_DOOR_CLOSE_TIMEOUT           6 
#define TR_PASSWORD_SUCCESS             7
#define TR_PASSWORD_WRONG               8
#define TR_BUTTON_PRESSED               9
#define TR_DISPLAY_TIMEDOUT             10
#define TR_CHANGE_PWD_SUCCESS           11
#define TR_CHANGE_PWD_WRONG             12
#define TR_SYSTEM_LOCKED                13
#define TR_MOVE_MOTOR_UNLOCK_DIR        14
#define TR_MOVE_MOTOR_LOCK_DIR          15
#define TR_STOP_MOTOR                   16
#define TR_EXIT_ADMIN_MODE              17

#define ST_LOCKED             1
#define ST_UNLOCKING          2
#define ST_UNLOCKED           3
#define ST_DOOR_CLOSE_TIMEOUT 4
#define ST_LOCKING            5
#define ST_WRONG_PWD_ENTER    6
#define ST_CHANGE_PWD_WRONG   7
#define ST_CHANGE_PWD_SUCCESS 8
#define ST_INITIAL            9
#define ST_SYSTEM_LOCKED      10
#define ST_ADMIN_MODE         11
#define ST_MOVE_M_FOR_UNLOCK  12
#define ST_MOVE_M_FOR_LOCK    13

enum LockStates{ LOCKED = 0 , MIDDLE = 1 , UNLOCKED = 2 };

//Events
void loginButtonClicked(void* ptr);
void changePwdButtonClicked(void* ptr);
void onCommandReceived(uint8_t code);
void buttonTimedout();
void doorLockTimedout();
void adminModeSessionEnd();

//State locked
void onLockingEnter();
void onLockingAction();
void onLockingExit();
void onLockedEnter();
void onLockedAction();
void onUnlockingEnter();
void onUnlockingAction();
void onUnlockingExit();
void onUnlockedEnter();
void onUnlockedAction();
void onDoorCloseTimedoutEnter();
void onDoorCloseTimedoutAction();
void onDoorCloseTimedoutExit();
void onWrongPasswordEnteredEnter();
void onWrongPasswordEnteredAction();
void onChangePwdSuccessEnter();
void onChangePwdSuccessAction();
void onChangePwdWrongEnter();
void onChangePwdWrongAction();
void onSystemLockedEnter();
void onSystemLockedAction();
void onInitialEnter();
void onInitialAction();

void onAdminModeEnter();
void onMoveMotorForUnlockEnter();
void onMoveMotorForUnlockExit();
void onMoveMotorForLockEnter();
void onMoveMotorForLockExit();

Scheduler   scheduler;
Task buttonTimeoutTask( BUTTON_REENABLE_TIMEOUT , TASK_ONCE , &buttonTimedout , &scheduler );
Task doorLockTimeoutTask( DOOR_LOCK_TIMEOUT , TASK_ONCE , &doorLockTimedout , &scheduler );
Task adminModeTimeoutTask( DEVELOPER_MODE_SESSION_DUR , TASK_ONCE ,&adminModeSessionEnd , &scheduler );
NexPage homePage = NexPage(0 , 0 , "lockedPage");
NexPage unlockedPage = NexPage(2 , 0 , "unlockedPage");
NexPage unlockingPage = NexPage(3 , 0 , "unlockingPage");
NexPage pwdWrongPage = NexPage(4 , 0 , "page4");
NexPage lockingPage = NexPage(5 , 0 , "lockingPage");
NexPage successPage = NexPage(12 , 0 , "pwdChanged");
NexPage rdDisabledPage = NexPage(13 , 0 , "rdDisabled");
NexButton loginButton = NexButton(1 , 14 , "b11");
NexButton changePwdButton = NexButton(9 , 13 , "b11");
NexText passwordTxt = NexText(0 , 2 , "t0");
NexText newPwd = NexText(9 , 1 , "t0");
NexVariable currentPwd = NexVariable(9 , 1 , "oldPwd");
uint8_t wrongPwdCount = 0;

State stateLocked(ST_LOCKED ,&onLockedEnter , &onLockedAction , NULL);
State stateUnlocking(ST_UNLOCKING , &onUnlockingEnter , &onUnlockingAction , &onUnlockingExit);
State stateUnlocked(ST_UNLOCKED , &onUnlockedEnter , &onUnlockedAction , NULL);
State stateDoorCloseTimedout(ST_DOOR_CLOSE_TIMEOUT , &onDoorCloseTimedoutEnter , &onDoorCloseTimedoutAction , &onDoorCloseTimedoutExit);
State stateLocking(ST_LOCKING , &onLockingEnter , &onLockingAction , &onLockingExit);
State stateWrongPasswordEntered(ST_WRONG_PWD_ENTER , &onWrongPasswordEnteredEnter , &onWrongPasswordEnteredAction , NULL);
State stateChangePwdWrong(ST_CHANGE_PWD_WRONG , &onChangePwdWrongEnter , &onChangePwdWrongAction , NULL );
State stateChangePwdSuccess(ST_CHANGE_PWD_SUCCESS , &onChangePwdSuccessEnter , &onChangePwdSuccessAction , NULL );
State stateSystemLocked(ST_SYSTEM_LOCKED , &onSystemLockedEnter , &onSystemLockedAction , NULL );
State stateAdminMode(ST_ADMIN_MODE , &onAdminModeEnter , NULL , NULL );
State stateMoveMotorForUnlock(ST_MOVE_M_FOR_UNLOCK , &onMoveMotorForUnlockEnter , NULL , &onMoveMotorForUnlockExit );
State stateMoveMotorForLock(ST_MOVE_M_FOR_LOCK , &onMoveMotorForLockEnter , NULL ,  &onMoveMotorForLockExit );
State stateInitial(ST_INITIAL , &onInitialEnter , &onInitialAction , NULL );

SimpleSerial host(&HOST_SERIAL , onCommandReceived);

volatile Fsm fsm(&stateInitial);

NexTouch *nex_listen_list[] = 
{
  &loginButton ,
  &changePwdButton ,
  NULL
};

volatile bool buttonEnableFlag = true;
volatile bool buttonPressed = false;
 
void setup() {
//  HMISerial.begin(9600);
//  Serial.begin(9600);
  nexInit();
  storageInit();
  doorInit();
  motorInit();

//  uint8_t key[] = { 5,1,4,3,7,14,8,9,1,2,24,22,28,94,110,85,83,127,16,54,61,32,43,44,55,78,92,10,8,5,94 };
//
//  String str = retrievePassword();
//  char  *pwd = str.c_str();
//  Serial.print("Pwd => ");
//  Serial.println(pwd);
//  char *copied = malloc((strlen(pwd) * sizeof(char)) + 1);
//  strcpy(copied , pwd);
//  aes256_enc_single(key , copied);
//  Serial.print("\nEncrypted => ");
//  Serial.println(copied);
//  aes256_dec_single(key , copied);
//  Serial.print("decrypted => ");
//  Serial.println(copied);

  HOST_SERIAL.begin(9600);
  HOST_SERIAL.println(F("R&D Steel Doors v1.0"));
  
  pinMode(2 , INPUT); 

  loginButton.attachPop(loginButtonClicked , &loginButton);
  changePwdButton.attachPop(changePwdButtonClicked , &changePwdButton);

  //Attaching transitions
  fsm.add_transition(&stateLocked , &stateUnlocking , TR_BUTTON_PRESSED , NULL );
  fsm.add_transition(&stateLocked , &stateUnlocking , TR_PASSWORD_SUCCESS , NULL );
//  fsm.add_transition(&stateLocked , &stateLocking , TR_LOCK_SWITCH_FALSE , NULL );
  fsm.add_transition(&stateLocked , &stateWrongPasswordEntered , TR_PASSWORD_WRONG , NULL );
  fsm.add_transition(&stateLocked , &stateSystemLocked , TR_SYSTEM_LOCKED , NULL );
  fsm.add_transition(&stateWrongPasswordEntered , &stateLocked , TR_DISPLAY_TIMEDOUT , NULL );
  fsm.add_transition(&stateUnlocking , &stateUnlocked , TR_UNLOCK_SWITCH_TRUE , NULL);
//  fsm.add_transition(&stateUnlocking , &stateLocking , TR_BUTTON_PlsRESSED , NULL );
//  fsm.add_transition(&stateUnlocked , &stateUnlocking , TR_UNLOCK_SWITCH_FALSE , NULL );
  fsm.add_transition(&stateUnlocked , &stateDoorCloseTimedout , TR_DOOR_CLOSE , NULL );
  fsm.add_transition(&stateUnlocked , &stateChangePwdSuccess , TR_CHANGE_PWD_SUCCESS , NULL);
  fsm.add_transition(&stateUnlocked , &stateChangePwdWrong , TR_CHANGE_PWD_WRONG , NULL );
  fsm.add_transition(&stateChangePwdSuccess , &stateUnlocked , TR_DISPLAY_TIMEDOUT , NULL );
  fsm.add_transition(&stateChangePwdWrong , &stateUnlocked , TR_DISPLAY_TIMEDOUT , NULL );
  fsm.add_transition(&stateDoorCloseTimedout , &stateLocking , TR_DOOR_CLOSE_TIMEOUT , NULL );
  fsm.add_transition(&stateDoorCloseTimedout , &stateLocking , TR_BUTTON_PRESSED , NULL );
  fsm.add_transition(&stateDoorCloseTimedout , &stateUnlocked , TR_DOOR_OPEN , NULL );
  fsm.add_transition(&stateLocking , &stateLocked , TR_LOCK_SWITCH_TRUE , NULL );
//  fsm.add_transition(&stateLocking , &stateUnlocking , TR_BUTTON_PRESSED , NULL );
//  fsm.add_transition(&stateLocking , &stateUnlocking , TR_DOOR_OPEN , NULL );
  fsm.add_transition(&stateSystemLocked , &stateLocked , TR_DISPLAY_TIMEDOUT , NULL );
  fsm.add_transition(&stateInitial , &stateDoorCloseTimedout , TR_DOOR_CLOSE , NULL );

  //Admin
//  fsm.add_transition(&stateAdminMode , &stateMoveMotorForUnlock , TR_MOVE_MOTOR_UNLOCK_DIR , NULL );
//  fsm.add_transition(&stateAdminMode , &stateMoveMotorForLock , TR_MOVE_MOTOR_LOCK_DIR , NULL );
//  fsm.add_transition(&stateAdminMode , &stateInitial ,TR_EXIT_ADMIN_MODE , NULL );
//  fsm.add_transition(&stateMoveMotorForUnlock , &stateAdminMode , TR_STOP_MOTOR , NULL );
//  fsm.add_transition(&stateMoveMotorForUnlock , &stateMoveMotorForLock , TR_MOVE_MOTOR_LOCK_DIR , NULL );
//  fsm.add_transition(&stateMoveMotorForLock , &stateAdminMode , TR_STOP_MOTOR , NULL );
//  fsm.add_transition(&stateMoveMotorForLock , &stateMoveMotorForUnlock , TR_MOVE_MOTOR_UNLOCK_DIR , NULL );
  
  //Interrupt
  attachInterrupt(digitalPinToInterrupt(2) , &buttonInterrupt ,  RISING);
}

void loop() {
  // put your main code here, to run repeatedly:
  nexLoop(nex_listen_list);
  fsm.run_machine();

  scheduler.execute();
  host.check();
  
  if(buttonPressed){
    buttonPressed = false;
    fsm.trigger(TR_BUTTON_PRESSED);
  }
}

void buttonInterrupt(){
  if(!buttonEnableFlag)
    return;

  buttonEnableFlag = false;
  buttonPressed = true;
  
  buttonTimeoutTask.restartDelayed();
}

void onCommandReceived(uint8_t code){
  switch(code){
    case CMD_LOGIN  :{

      uint32_t remaining;
      //If login is blocked
      if(remaining = is_blocked()){
        HOST_SERIAL.write('{');
        HOST_SERIAL.write(CMD_LOGIN);
        HOST_SERIAL.print(2);//Login blocked code
        HOST_SERIAL.write(',');
        HOST_SERIAL.print(remaining);
        HOST_SERIAL.write('}');
        fsm.trigger(TR_SYSTEM_LOCKED);
        break;
      }                                                                                                                               
      
      //Read pwd
      String pwdStr = host.readStrArg();
      //Retrieve pwd
      String currPwd = retrievePassword();

      uint8_t res = strcmp(currPwd.c_str() , pwdStr.c_str());
      
      if(res == 0){
        //Success
        fsm.trigger(TR_PASSWORD_SUCCESS);
        HOST_SERIAL.write('{');
        HOST_SERIAL.write(CMD_LOGIN);
        HOST_SERIAL.print(1);
        HOST_SERIAL.write('}');
      } else {
        HOST_SERIAL.write('{');
        HOST_SERIAL.write(CMD_LOGIN);
        HOST_SERIAL.print(0);
        HOST_SERIAL.write('}');
        //Pwd wrong has been attempted
        wrong_attempt();
      }
    }
    break;
    case CMD_GET_STATE  : {
       HOST_SERIAL.write('{');
       HOST_SERIAL.write(CMD_GET_STATE);
       HOST_SERIAL.print(fsm.current_state()->id);
       HOST_SERIAL.write('}');
    }
    break;
    case CMD_GET_VERSION: {
      HOST_SERIAL.write('{');
      HOST_SERIAL.write(CMD_GET_VERSION);
      HOST_SERIAL.print(F("R&G Steel Doors v1.0"));
      HOST_SERIAL.write('}');
    break;
    }
    case CMD_MOVE_MOTOR_FOR_UNLOCK: {
      fsm.trigger(TR_MOVE_MOTOR_UNLOCK_DIR);
    }
    break;
    case CMD_MOVE_MOTOR_FOR_LOCK:{
      fsm.trigger(TR_MOVE_MOTOR_LOCK_DIR);
    }
    break;
    case CMD_EXIT_ADMIN_MODE: {
      fsm.trigger(TR_EXIT_ADMIN_MODE);
    }
    break;
    case CMD_STOP_MOTOR : {
      fsm.trigger(TR_STOP_MOTOR);
    }
  }
}
void buttonTimedout(){
  //Button re-enabling
  buttonEnableFlag = true;
}
void loginButtonClicked(void* ptr){
  uint32_t remaining;
  if(remaining = is_blocked()){
    fsm.trigger(TR_SYSTEM_LOCKED);
    return;
  }
  char str[12];
  uint16_t acLen = passwordTxt.getText(str , 12);
  str[acLen] = '\0';

  String pwdStr = retrievePassword();
  char* pwd = pwdStr.c_str();

  uint8_t res = strcmp(str , pwd);
  if(res == 0){
    //Success
    fsm.trigger(TR_PASSWORD_SUCCESS);
  } else {
    //Pwd wrong has been attempted
    wrong_attempt();
    //Fail
    fsm.trigger(TR_PASSWORD_WRONG);
  }
}
void changePwdButtonClicked(void* ptr){
  char enteredPwdStr[12];
  char currPwdStr[12];
  uint16_t newPwdLen = newPwd.getText(enteredPwdStr , 12);
  uint16_t currPwdLen = currentPwd.getText(currPwdStr , 12);
  enteredPwdStr[newPwdLen] = '\0';
  currPwdStr[currPwdLen] = '\0';

  //Retrieve password
  String pwd = retrievePassword();

  uint8_t res = strcmp(pwd.c_str() , currPwdStr);

  if(res != 0){
    //Success
    fsm.trigger(TR_CHANGE_PWD_WRONG);
    return;
  }

  //Save password
  savePassword(String(enteredPwdStr));

  //Pwd changed
  fsm.trigger(TR_CHANGE_PWD_SUCCESS);
  
  
//  Serial.println("Changed Pwd Button Clicked");
}
void onLockingEnter(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onLockingEnter"));
  #endif
  lockingPage.show();
  motorDir(false);
  motorEnable(true);  
}
void onLockingAction(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onLockingAction"));
  #endif
  //If the door is opened
//  if(!doorIsClosed()){
//    fsm.trigger(TR_DOOR_OPEN);
//    return;
//  }

  //If is locked
  if(isLocked()){
    fsm.trigger(TR_LOCK_SWITCH_TRUE);
    return;
  }
}
void onLockingExit(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onLockingExit"));
  #endif
  motorEnable(false);
}

void onLockedEnter(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onLockedEnter"));
  #endif
  //Display locked screen
  homePage.show();
}

void onLockedAction(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onLockedAction"));
  #endif
//  if(!isLocked()){
//    fsm.trigger(TR_LOCK_SWITCH_FALSE);
//  }
}
void onWrongPasswordEnteredEnter(){
  //Displaying
//  Serial.println(F("Wrong Password Entered"));/
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onWrongPasswordEnteredEnter"));
  #endif
  pwdWrongPage.show();
  delay(2000);
  //fsm.trigger(TR_DISPLAY_TIMEDOUT);
}
void onWrongPasswordEnteredAction(){
    fsm.trigger(TR_DISPLAY_TIMEDOUT);

}
void onUnlockingEnter(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onUnlockingEnter"));
  #endif
//  Serial.println(F("Unlocking State "));/
  unlockingPage.show();
  motorDir(true);
  motorEnable(true);
}
void onUnlockingAction(){
    #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onUnlockingAction"));
  #endif
  //If the door is unlocked
  if(isUnlocked())
    fsm.trigger(TR_UNLOCK_SWITCH_TRUE);
}
void onUnlockingExit(){
      #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onUnlockingExit"));
  #endif
  motorEnable(false);
}
void onUnlockedEnter(){
        #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onUnlockedEnter"));
  #endif
  //Display unlocked page
  unlockedPage.show();
}
void onUnlockedAction(){
          #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onUnlockedAction"));
  #endif
  //If is unlocked
//  if(!isUnlocked()){
//    fsm.trigger(TR_UNLOCK_SWITCH_FALSE);
//    return;
//  }

  //If the door is closed
  if(doorIsClosed()){
    fsm.trigger(TR_DOOR_CLOSE);
    return;
  }
}
void onDoorCloseTimedoutEnter(){
//  Serial.println(F("DoorCloseTimeout State "));/
          #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onDoorCloseTimedoutEnter"));
  #endif
  doorLockTimeoutTask.restartDelayed();
}
void onDoorCloseTimedoutAction(){
  if(!doorIsClosed()){
    fsm.trigger(TR_DOOR_OPEN);
  }
}
void onDoorCloseTimedoutExit(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onDoorCloseTimedoutExit"));
  #endif
  doorLockTimeoutTask.disable();
}
void onChangePwdWrongEnter(){
 #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onChangePwdWrongEnter"));
  #endif
  pwdWrongPage.show();
  delay(2000);
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onChangePwdWrongExit"));
  #endif
  fsm.trigger(TR_DISPLAY_TIMEDOUT);
}
void onChangePwdWrongAction(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onChangePwdWrongAction"));
  #endif
  fsm.trigger(TR_DISPLAY_TIMEDOUT);
}
void onChangePwdSuccessEnter(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onChangePwdSuccessEnter"));
  #endif
  successPage.show();
  delay(2000);
  fsm.trigger(TR_DISPLAY_TIMEDOUT);
}
void onChangePwdSuccessAction(){
    #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onChangePwdSuccessAction"));
  #endif
  fsm.trigger(TR_DISPLAY_TIMEDOUT);
}
void onSystemLockedEnter(){
  rdDisabledPage.show();
  delay(is_blocked());
  fsm.trigger(TR_DISPLAY_TIMEDOUT);
}
void onSystemLockedAction(){
  fsm.trigger(TR_DISPLAY_TIMEDOUT);
}
void onInitialEnter(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onInitialEnter"));
  #endif
  homePage.show();
}
void onInitialAction(){
  #ifdef STATE_DEBUGGING
    dbgSerial.println(F("onInitialAction"));
  #endif
  //If door is closed
  if(doorIsClosed()){
    fsm.trigger(TR_DOOR_CLOSE);
  }
}

//Admin mode states

void onAdminModeEnter(){
//  Serial.println(F("Admin Mode"));
}

void onMoveMotorForUnlockEnter(){
//  Serial.println(F("Unlock Move Motor"));
  motorDir(true);
  motorEnable(true);
}

void onMoveMotorForUnlockExit(){
  motorEnable(false);
}

void onMoveMotorForLockEnter(){
//  Serial.println(F("Lock Move Motor"));
  motorDir(false);
  motorEnable(true);
}

void onMoveMotorForLockExit(){
  motorEnable(false);
}

void doorLockTimedout(){
  fsm.trigger(TR_DOOR_CLOSE_TIMEOUT);
}

void adminModeSessionEnd(){
  fsm.trigger(TR_EXIT_ADMIN_MODE);
}

