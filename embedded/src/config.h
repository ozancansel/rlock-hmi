#ifndef CONFIG_H
#define CONFIG_H

#define MAX_PASSWORD_LENGTH         16
#define DOOR_LOCK_TIMEOUT           15000
#define BUTTON_REENABLE_TIMEOUT     800
#define PWD_ATTEMPT_MAX             3
#define PWD_ATTEMPT_TIMEOUT         60000
#define DEVELOPER_MODE_SESSION_DUR  600000UL

#define BUTTON_OPEN               2
#define MOTOR_IN1                 7
#define MOTOR_IN2                 8
#define MOTOR_PWM                 11

#define DOOR_CLOSED_SWITCH        A0
#define DOOR_UNLOCKED_SWITCH      A2
#define DOOR_LOCKED_SWITCH        A1

#define DOOR_CLOSE_LOGIC_TRUE     1
#define UNLOCK_LOGIC_TRUE         0
#define LOCK_LOGIC_TRUE           1

#endif
