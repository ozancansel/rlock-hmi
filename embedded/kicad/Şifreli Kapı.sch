EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ATMEGA328P-PU
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L7805 U1
U 1 1 5A20F455
P 2750 2400
F 0 "U1" H 2600 2525 50  0000 C CNN
F 1 "L7805" H 2750 2525 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 2775 2250 50  0001 L CIN
F 3 "" H 2750 2350 50  0001 C CNN
	1    2750 2400
	1    0    0    -1  
$EndComp
$Comp
L ATMEGA328P-PU U2
U 1 1 5A20F4EA
P 4600 2750
F 0 "U2" H 3850 4000 50  0000 L BNN
F 1 "ATMEGA328P-PU" H 5000 1350 50  0000 L BNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 4600 2750 50  0001 C CIN
F 3 "" H 4600 2750 50  0001 C CNN
	1    4600 2750
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x04_Male J1
U 1 1 5A20F527
P 2700 3250
F 0 "J1" H 2700 3450 50  0000 C CNN
F 1 "HC-05" H 2700 2950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 2700 3250 50  0001 C CNN
F 3 "" H 2700 3250 50  0001 C CNN
	1    2700 3250
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J2
U 1 1 5A20F5F4
P 6150 1950
F 0 "J2" H 6150 2050 50  0000 C CNN
F 1 "Motor" H 6150 1750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6150 1950 50  0001 C CNN
F 3 "" H 6150 1950 50  0001 C CNN
	1    6150 1950
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03_Male J3
U 1 1 5A20F667
P 6150 2650
F 0 "J3" H 6150 2850 50  0000 C CNN
F 1 "Kapı Kapalı Sensör" H 6150 2450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6150 2650 50  0001 C CNN
F 3 "" H 6150 2650 50  0001 C CNN
	1    6150 2650
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03_Male J4
U 1 1 5A20F6DB
P 6200 3200
F 0 "J4" H 6200 3400 50  0000 C CNN
F 1 "Kilit Kapalı Sensör" H 6200 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6200 3200 50  0001 C CNN
F 3 "" H 6200 3200 50  0001 C CNN
	1    6200 3200
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03_Male J5
U 1 1 5A20F723
P 6250 3800
F 0 "J5" H 6250 4000 50  0000 C CNN
F 1 "Kilit Açık Sensör" H 6250 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6250 3800 50  0001 C CNN
F 3 "" H 6250 3800 50  0001 C CNN
	1    6250 3800
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x04_Male J6
U 1 1 5A20F761
P 7400 1800
F 0 "J6" H 7400 2000 50  0000 C CNN
F 1 "Ekran" H 7400 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 7400 1800 50  0001 C CNN
F 3 "" H 7400 1800 50  0001 C CNN
	1    7400 1800
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J7
U 1 1 5A20F7DA
P 7500 2600
F 0 "J7" H 7500 2700 50  0000 C CNN
F 1 "Kilit Açma Buton" H 7500 2400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 7500 2600 50  0001 C CNN
F 3 "" H 7500 2600 50  0001 C CNN
	1    7500 2600
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x14_Male J9
U 1 1 5A20F9BA
P 8350 2050
F 0 "J9" H 8350 2750 50  0000 C CNN
F 1 "Çoğaltma Sol" H 8350 1250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14_Pitch2.54mm" H 8350 2050 50  0001 C CNN
F 3 "" H 8350 2050 50  0001 C CNN
	1    8350 2050
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x14_Male J8
U 1 1 5A20FA94
P 7800 3550
F 0 "J8" H 7800 4250 50  0000 C CNN
F 1 "Çoğaltma Sağ" H 7800 2750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14_Pitch2.54mm" H 7800 3550 50  0001 C CNN
F 3 "" H 7800 3550 50  0001 C CNN
	1    7800 3550
	1    0    0    -1  
$EndComp
$Comp
L C Kristal2
U 1 1 5A20FB18
P 3200 1750
F 0 "Kristal2" H 3225 1850 50  0000 L CNN
F 1 "C" H 3225 1650 50  0000 L CNN
F 2 "Capacitors_THT:C_Axial_L5.1mm_D3.1mm_P7.50mm_Horizontal" H 3238 1600 50  0001 C CNN
F 3 "" H 3200 1750 50  0001 C CNN
	1    3200 1750
	1    0    0    -1  
$EndComp
$Comp
L C Kristal1
U 1 1 5A20FBBB
P 2900 1750
F 0 "Kristal1" H 2925 1850 50  0000 L CNN
F 1 "C" H 2925 1650 50  0000 L CNN
F 2 "Capacitors_THT:C_Axial_L5.1mm_D3.1mm_P7.50mm_Horizontal" H 2938 1600 50  0001 C CNN
F 3 "" H 2900 1750 50  0001 C CNN
	1    2900 1750
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5A20FCC4
P 3300 2750
F 0 "R1" V 3380 2750 50  0000 C CNN
F 1 "R" V 3300 2750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3230 2750 50  0001 C CNN
F 3 "" H 3300 2750 50  0001 C CNN
	1    3300 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
